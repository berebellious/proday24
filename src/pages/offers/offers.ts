import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, App } from 'ionic-angular';
import { SuperTabsController } from "../../ionic2-super-tabs/src";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { DataServiceProvider } from "../../providers/data-service/data-service";
import { Subscription } from "rxjs";
import { RegistrationModalPage } from "../registration-modal/registration-modal";


@Component({
  selector: 'page-offers',
  templateUrl: 'offers.html',
})
export class OffersPage {

  isLoggedIn: boolean = false;
  private userSubscription: Subscription;
  private modalSubscription: Subscription;

  page1: any = 'SellingPage';
  page2: any = 'BuyingPage';
  page3: any = 'BoardsPage';

  showIcons: boolean = true;
  showTitles: boolean = true;
  pageTitle: string = 'Full Height';
  userId;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private superTabsCtrl: SuperTabsController,
    private auth: AuthServiceProvider,
    private dataService: DataServiceProvider,
    public app: App,
    public modalCtrl: ModalController) {

    const type = navParams.get('type');
    switch (type) {
      case 'icons-only':
        this.showTitles = false;
        this.pageTitle += ' - Icons only';
        break;

      case 'titles-only':
        this.showIcons = false;
        this.pageTitle += ' - Titles only';
        break;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OffersPage');
  }


  onTabSelect(tab: { index: number; id: string; }) {
    console.log(`Selected tab: `, tab);
  }

  ionViewWillEnter() {
    this.userSubscription = this.auth.isLoginSubject
      .subscribe(resp => {
        console.log("listening from offers", resp)
        if (resp) {
          this.isLoggedIn = true;
          this.userId = resp.userId
        } else {
          this.isLoggedIn = false
          this.testModal()
        }
      })
  }

  ionViewWillLeave() {
    console.log("unsubscribing from offers page")
    this.dataService.sellingDataLoaded = false;
    this.dataService.buyingData = null;
    if (this.userSubscription) this.userSubscription.unsubscribe()
    if (this.modalSubscription) this.modalSubscription.unsubscribe()
  }

  testModal() {
    let modal = this.modalCtrl.create(RegistrationModalPage, { fromTabs: true })
    modal.present()
    modal.onWillDismiss(
      () => {
        this.modalSubscription = this.auth.isLoginSubject
          .subscribe(resp => {
            console.log(resp)
            if (resp == null) {
              this.isLoggedIn = false
              this.app.getRootNav().getActiveChildNav().select(0);
            } else {
              this.isLoggedIn = true;
            }
          })
      }
    )
  }

}
