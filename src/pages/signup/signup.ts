import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ViewController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthServiceProvider } from "../../providers/providers";
import { EmailValidator } from "../../validators/email";
import { RegistrationModalPage } from "../registration-modal/registration-modal";


@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class Signup {
  submitAttempt: boolean = false
  signupForm: FormGroup
  // registration: RegistrationModalPage

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private auth: AuthServiceProvider,
    private registration: RegistrationModalPage,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController) {

    this.signupForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      firstName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      phone: ['', Validators.compose([Validators.maxLength(12), Validators.minLength(12), Validators.pattern('[0-9]*'), Validators.required])]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Signup');
  }

  login() {
    // this.registration.loginContent()
    this.registration.landingContent()
  }

  signup() {
    let loader = this.loadingCtrl.create()
    this.submitAttempt = true
    if (!this.signupForm.valid) {

    } else {
      loader.present()
      this.auth.signup(this.signupForm.value)
        .subscribe(async resp => {
          if (resp && resp.data.success) {
            await this.auth._initialiseUser(resp.data, null, false);
            loader.dismiss()
            this.registration.fromTabs ? this.registration.cancel() : this.registration.skip()
            // this.viewCtrl.dismiss()
            console.log(resp);
          } else {
            loader.dismiss()
            console.error(resp);
            let alert = this.alertCtrl.create({
              title: "Error",
              subTitle: resp.data.message,
              buttons: ["OK"]
            });
            alert.present()
          }
        }, error => {
          console.error(error);
        })
    }
  }

  lsCancel() {
    this.registration.lsCancel()
    this.signupForm.reset()
  }

}
