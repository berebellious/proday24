import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PostingServiceProvider } from "../../providers/posting-service/posting-service";


@IonicPage()
@Component({
  selector: 'page-ad-price',
  templateUrl: 'ad-price.html',
})
export class AdPrice {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private postingService: PostingServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdPrice');
  }

  newPost() {
    this.postingService.newPost()
  }

  newImage() {
    this.postingService.picupload()
  }

}
