import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdPrice } from './ad-price';

@NgModule({
  declarations: [
    AdPrice,
  ],
  imports: [
    IonicPageModule.forChild(AdPrice),
  ],
  exports: [
    AdPrice
  ]
})
export class AdPriceModule {}
