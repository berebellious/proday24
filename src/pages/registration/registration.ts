import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController, Tabs, App } from 'ionic-angular';
import { RegistrationModalPage } from "../registration-modal/registration-modal";


@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {
  hideLogin: boolean = true
  hideSignup: boolean = true
  hideRegistration: boolean = false

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public tabs: Tabs,
    public app: App
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationPage');
  }

  cancel() {
    this.viewCtrl
      .dismiss()
      .then(() => {
        this.tabs = this.navCtrl.parent
        // this.navCtrl.parent.select(0)
        // this.tabs.select(1)
        this.app.getRootNav().getActiveChildNav().select(1);
      })

  }

  register() {
    let modal = this.modalCtrl.create(RegistrationModalPage)
    modal.present()
  }

  public lsCancel() {
    this.hideLogin = true
    this.hideSignup = true
    this.hideRegistration = false
  }

  loginContent() {
    this.hideLogin = false
    this.hideSignup = true
    this.hideRegistration = true
  }

  signupContent() {
    this.hideLogin = true
    this.hideSignup = false
    this.hideRegistration = true
  }


}
