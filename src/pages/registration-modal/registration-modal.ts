import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ViewController, App, Platform } from 'ionic-angular';


@Component({
  selector: 'page-registration-modal',
  templateUrl: 'registration-modal.html',
})
export class RegistrationModalPage {
  @ViewChild('input') myInput: ElementRef

  hideLogin: boolean = true
  hideSignup: boolean = true
  hideRegistration: boolean = false
  fromTabs: boolean = false

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public app: App,
    public platform: Platform) {

    this.fromTabs = navParams.get('fromTabs')
    // if (platform.is('android')) {
    //   platform.registerBackButtonAction(() => {
    //     this.cancel()
    //   }, 1)
    // }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationModalPage');
    // this.myInput.nativeElement.setFocus()
  }

  cancel() {
    this.viewCtrl.dismiss()
  }
  skip() {
    this.navCtrl.setRoot('TabsPage')
  }

  public lsCancel() {
    this.hideLogin = true
    this.hideSignup = true
    // this.hideRegistration = false
  }

  loginContent() {
    this.hideLogin = false
    this.hideSignup = true
    this.hideRegistration = true
  }

  signupContent() {
    this.hideLogin = true
    this.hideSignup = false
    this.hideRegistration = true
  }

  landingContent() {
    this.hideLogin = true
    this.hideSignup = true
    this.hideRegistration = false
  }

}
