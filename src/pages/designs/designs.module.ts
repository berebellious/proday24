import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Designs } from './designs';

@NgModule({
  declarations: [
    Designs,
  ],
  imports: [
    IonicPageModule.forChild(Designs),
  ],
  exports: [
    Designs
  ]
})
export class DesignsModule {}
