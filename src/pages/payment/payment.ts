import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { DataServiceProvider } from "../../providers/data-service/data-service";
import { Stripe, StripeCardTokenParams } from "@ionic-native/stripe";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  bumpContent: boolean = true;
  paymentContent: boolean = false;
  submitAttempt: boolean = false;
  offerImage: string;
  offerId;
  stripeForm: FormGroup;
  maxYear; minYear;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public dS: DataServiceProvider,
    public stripe: Stripe,
    public formBuilder: FormBuilder,
    public navParams: NavParams) {

    this.offerImage = navParams.get('offerImage');
    this.offerId = navParams.get('offerId');

    this.stripe.setPublishableKey('pk_test_J7eItXPhWj6f2XekzZ8cTrdl');

    let currentYear = new Date().getFullYear();
    this.maxYear = currentYear + 10;
    this.minYear = currentYear;
    console.log(this.maxYear);
    console.log(this.minYear);

    this.stripeForm = formBuilder.group({
      cardNumber: ['', Validators.compose([Validators.pattern('[0-9]{16}'), Validators.required])],
      cvc: ['', Validators.compose([Validators.pattern('[0-9]{3}'), Validators.required])],
      expDate: ['', Validators.required],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }

  bump() {
    this.bumpContent = false;
    this.paymentContent = true;
  }

  async pay() {
    let loader = this.loadingCtrl.create({
      content: "Making Payment of $25"
    });
    loader.present();
    try {
      this.submitAttempt = true;
      console.log(this.stripeForm.value);
      const token = await this.createNewSource();
      const resp = await this.dS.makePayment(25, token.id, this.offerId).toPromise();
      console.log(resp);
      if (resp.data.success !== true) { throw resp.data.message;}
      await loader.dismiss();
      await this.successToast();
      this.viewCtrl.dismiss('success');
    } catch (error) {
      console.error('error during payment', error);
      await loader.dismiss();
      await this.errorToast();
    }

  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  async createNewSource() {
    try {
      let expYear = this.stripeForm.value.expDate.substring(0, this.stripeForm.value.expDate.lastIndexOf('-'));
      let expMonth = this.stripeForm.value.expDate.substring(this.stripeForm.value.expDate.lastIndexOf('-') + 1);
      console.log(expYear, expMonth);
      let card: StripeCardTokenParams = {
        number: this.stripeForm.value.cardNumber,
        expMonth: parseInt(expMonth),
        expYear: parseInt(expYear),
        cvc: this.stripeForm.value.cvc
      }
      const token = await this.stripe.createCardToken(card)
      console.log(token);
      return token;
    } catch (error) {
      throw error;
    }
  }

  successToast() {
    let toast = this.toastCtrl.create({
      message: 'Payment successfull',
      duration: 3000,
      position: 'bottom',
      showCloseButton: true,
      closeButtonText: 'OK',
    });
    return toast.present();
  }
  errorToast() {
    let toast = this.toastCtrl.create({
      message: 'Error in Payment process',
      duration: 3000,
      position: 'bottom',
      showCloseButton: true,
      closeButtonText: 'OK',
    });
    return toast.present();
  }
}
