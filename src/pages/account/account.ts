import { Component } from '@angular/core';
// import { DomSanitizer } from '@angular/platform-browser';
import { NavController, NavParams, ModalController, LoadingController, App, AlertController } from 'ionic-angular';
import { AuthServiceProvider, UserInfo } from "../../providers/auth-service/auth-service";
import { Subscription } from "rxjs";
import { RegistrationModalPage } from "../registration-modal/registration-modal";
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { UserServiceProvider } from "../../providers/user-service/user-service";
import { CameraServiceProvider } from "../../providers/camera-service/camera-service";


@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {
  langs = ['en', 'fr', 'es'];
  isLoggedIn: boolean = false;
  private userSubscription: Subscription;
  private modalSubscription: Subscription;
  private userInfoSubscription: Subscription;
  userId;
  private isLoggedOut: boolean = false;
  private userInfo: UserInfo;
  image: any;
  rating: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private auth: AuthServiceProvider,
    private cameraService: CameraServiceProvider,
    private userService: UserServiceProvider,
    public app: App,
    public translate: TranslateService,
    public modalCtrl: ModalController) {

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      console.log('Language changed to ' + this.translate.currentLang);
    });

    // this.getUserInfo()
    // this.userInfoSubscription = this.userService.getUserInfo()
    //   .subscribe(resp => {
    //     console.log('user info service', resp);
    //     this.rating = resp.data.profile.rating;
    //   });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');

  }

  ionViewWillEnter() {
    let loader = this.loadingCtrl.create({
      content: 'Loading Account Details'
    })
    loader.present()
    this.userSubscription = this.auth.isLoggedIn()
      .subscribe(resp => {
        console.log("listening from accounts page", resp)
        if (resp) {
          this.isLoggedIn = true;
          this.userId = resp.userId
          this.userInfo = resp;
          console.log(`${this.userInfo.image}`);
          this.image = `url(${this.userInfo.image})`;
          this.userInfoSubscription = this.userService.getUserInfo()
            .subscribe(resp => {
              console.log('user info service', resp);
              this.rating = resp.data.profile.rating;
            });
          loader.dismiss();
        } else {
          this.isLoggedIn = false
          this.testModal()
          loader.dismiss();
          // if (this.isLoggedOut) {
          // } else this.testModal()
        }
      })
  }

  updateUserInfo(data): UserInfo {
    let userInfo: UserInfo = {
      userId: data.user_id,
      firstName: data.first_name,
      lastName: data.last_name,
      email: data.email,
      phone: data.phone,
      dateCreated: data.date_created,
      image: data.image,
      socialLogin: this.auth.isLoginSubject.getValue().socialLogin
    }
    console.log('userInfo: ', userInfo)
    this.auth.updateLoginSubject(userInfo);
    this.auth.isLoggedIn().subscribe(resp => console.log('resp form subject', resp))
    return userInfo
  }

  getUserInfo() {

    this.userService.getUserInfo()
      .subscribe(resp => {
        console.log(resp)
        if (resp.data.profile) {
          this.userInfo = this.updateUserInfo(resp.data.profile);
          console.log(`${this.userInfo.image}`);
          this.image = `url(${this.userInfo.image})`;
        } else {
          throw resp
        }
      }, error => console.error(error))
  }

  ionViewWillLeave() {
    console.log("unsubscribing from accounts page")
    if (this.userSubscription) this.userSubscription.unsubscribe()
    if (this.modalSubscription) this.modalSubscription.unsubscribe()
    if (this.userInfoSubscription) this.userInfoSubscription.unsubscribe()
  }

  testModal() {
    let modal = this.modalCtrl.create(RegistrationModalPage, { fromTabs: true })
    modal.present()
    modal.onWillDismiss(
      () => {
        this.modalSubscription = this.auth.isLoggedIn()
          .subscribe(resp => {
            console.log(resp)
            if (resp == null) {
              this.isLoggedIn = false
              this.app.getRootNav().getActiveChildNav().select(0);
            } else {
              this.isLoggedIn = true;
            }
          })
      }
    );
  }

  async takeAPicture() {
    let loader = this.loadingCtrl.create({
      content: 'Updating photo'
    });
    try {
      let imageObject = await this.cameraService.takeAPicture()
      console.log("imageObject", imageObject);
      loader.present();
      const photoUpload = await this.userService.updatePhoto(imageObject)
      console.log(`${photoUpload}`);
      this.getUserInfo();
      loader.dismiss();
    } catch (error) {
      console.error(`${error}`)
      loader.dismiss();
    }
  }

  async chooseFromGallery() {
    let loader = this.loadingCtrl.create({
      content: 'Updating photo'
    });
    try {
      let imageObject = await this.cameraService.chooseFromGallery();
      console.log("imageObject", imageObject);
      loader.present();
      const photoUpload = await this.userService.updatePhoto(imageObject)
      console.log(`${photoUpload}`);
      this.getUserInfo()
      loader.dismiss();
    } catch (error) {
      console.error(`${error}`)
      loader.dismiss();
    }
  }

  takePhoto() {
    let alert = this.alertCtrl.create({
      title: 'Take a photo',
      buttons: [
        {
          text: 'Take A Picture',
          handler: () => {
            this.takeAPicture()
          }
        },
        {
          text: 'Choose From Gallery',
          handler: () => {
            this.chooseFromGallery()
          }
        }
      ]
    });
    alert.present()
  }

  async logout() {
    this.isLoggedOut = true
    let loader = this.loadingCtrl.create()
    loader.present()
    const resp = await this.auth.logout(this.userId)
    console.log(resp)
    loader.dismiss()
    this.userSubscription.unsubscribe()
    this.app.getRootNav().getActiveChildNav().select(0);

  }

  translateToEnglish() {
    this.translate.use('en');
  }

  translateToRussian() {
    this.translate.use('rus');
  }

  share() {
    let modal = this.modalCtrl.create('ShareModalPage', { title: 'Invite a Friend' });
    modal.present();
  }
}