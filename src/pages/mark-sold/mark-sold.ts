import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { DataServiceProvider } from "../../providers/data-service/data-service";
import { Subscription } from "rxjs";

@IonicPage()
@Component({
  selector: 'page-mark-sold',
  templateUrl: 'mark-sold.html',
})
export class MarkSoldPage {

  soldList = [];
  subscription: Subscription;
  selectedUser: any;
  user: any;
  list: boolean = false;
  item: any;
  offerId: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private dS: DataServiceProvider) {

    this.item = navParams.get('item');
    this.offerId = navParams.get('offerId');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MarkSoldPage');
  }
  ionViewWillEnter() {
    let offerId = this.navParams.get('offerId');
    console.log('offerId', offerId);
    this.subscription = this.dS.getMarkAsSoldList(offerId)
      .subscribe(resp => {
        console.log('resp', resp);
        if (resp.status == "OK" && resp.message !== "noData") {
          this.soldList = resp.data.list;
          console.log(this.soldList);
          this.user = resp.data.list[0].userId;
          this.selectedUser = resp.data.list[0];
          console.log('soldList', this.soldList);
          this.list = true;
        }
      })
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
    this.list = false;
  }

  select(selectedUser) {
    this.selectedUser = selectedUser;
    console.log('selectedUser', selectedUser);
  }

  submit() {
    let loader = this.loadingCtrl.create();
    loader.present();
    console.log('this.selectedUser', this.selectedUser);
    this.dS.sell(this.offerId, this.selectedUser.userId)
      .subscribe(resp => {
        console.log(resp);
        loader.dismiss().then(() => this.navCtrl.push('RateBuyerPage', { user: this.selectedUser, item: this.item }))
      })
  }

}
