import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MarkSoldPage } from './mark-sold';

@NgModule({
  declarations: [
    MarkSoldPage,
  ],
  imports: [
    IonicPageModule.forChild(MarkSoldPage),
  ],
  exports: [
    MarkSoldPage
  ]
})
export class MarkSoldPageModule {}
