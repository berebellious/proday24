import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-test-modal',
  templateUrl: 'test-modal.html',
})
export class TestModalPage {
  visible: string = "loginOrSignup"

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestModalPage');
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  lsCancel() {
    this.visible = "loginOrSignup"
  }

  login() {
    this.visible = "loginContent"
  }

  signup() {
    this.visible = "signupContent"
  }
}
