import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataServiceProvider } from "../../providers/data-service/data-service";


@IonicPage()
@Component({
  selector: 'page-report-two',
  templateUrl: 'report-two.html',
})
export class ReportTwoPage {

  report: string;
  offerId: any;
  text: string = '';
  itemDetails: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private dS: DataServiceProvider) {

    this.report = navParams.get('report');
    this.offerId = navParams.get('offerId');
    this.itemDetails = navParams.get('itemDetails')
    console.log(this.itemDetails);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportTwoPage');
  }

  postReport(text: string) {
    let report = this.report + ' ' + text
    this.dS.report(this.offerId, report)
    .subscribe(resp => {
      console.log(resp);
      this.navCtrl.pop();
    })
  }

}
