import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReportTwoPage } from './report-two';

@NgModule({
  declarations: [
    ReportTwoPage,
  ],
  imports: [
    IonicPageModule.forChild(ReportTwoPage),
  ],
  exports: [
    ReportTwoPage
  ]
})
export class ReportTwoPageModule {}
