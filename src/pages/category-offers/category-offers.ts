import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { DataServiceProvider } from "../../providers/data-service/data-service";
// import { LocationServiceProvider } from "../../providers/location-service/location-service";
import { Subscription } from "rxjs";


@IonicPage()
@Component({
  selector: 'page-category-offers',
  templateUrl: 'category-offers.html',
})
export class CategoryOffersPage {
  offerSubscription: Subscription;
  itemDetailSubscription: Subscription;
  offersList = [];
  category: any;
  noOffersYet: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private dataService: DataServiceProvider,
    // private locationService: LocationServiceProvider,
    public loadingCtrl: LoadingController) {

    this.category = navParams.get('category');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryOffersPage');
  }

  ionViewWillEnter() {
    let loader = this.loadingCtrl.create({
      content: 'Loading Offers'
    });
    loader.present();
    this.offerSubscription = this.dataService.getOffers(this.category, '0')
      .subscribe(
      resp => {
        console.log(resp);
        if (resp.data.isEmpty == true) {
          this.noOffersYet = true;
          loader.dismiss();
          this.offerSubscription.unsubscribe();
        } else {
          this.noOffersYet = false;
          this.offersList = resp.data.offersList;
          loader.dismiss();
          this.offerSubscription.unsubscribe();
        }
      }
      )
  }

  goToItemDetail(item) {
    console.log(item)
    this.navCtrl.push('ItemDetailPage', { item: item })
  }

  goToCategories() {
    this.navCtrl.pop()
  }

}
