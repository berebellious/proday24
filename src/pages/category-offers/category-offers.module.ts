import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoryOffersPage } from './category-offers';
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [
    CategoryOffersPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(CategoryOffersPage),
  ],
  exports: [
    CategoryOffersPage
  ]
})
export class CategoryOffersPageModule {}
