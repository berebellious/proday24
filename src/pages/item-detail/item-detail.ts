import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, LoadingController } from 'ionic-angular';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from "@angular/platform-browser";
import { DataServiceProvider } from "../../providers/data-service/data-service";
import { PostingServiceProvider } from "../../providers/posting-service/posting-service";


@IonicPage()
@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html',
})
export class ItemDetailPage {
  itemDetails: any;
  itemImages = []
  fromSelling: boolean = false;
  prevPage: string;
  isWatchlist: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private postService: PostingServiceProvider,
    private dS: DataServiceProvider,
    private _sanitizer: DomSanitizer) {

    let item = navParams.get('item');
    this.fromSelling = navParams.get('fromSelling');
    this.prevPage = navParams.get('prevPage');
    this.loadData(item);
    // console.log('item', item);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ItemDetailPage');
  }

  loadData(item) {
    let loader = this.loadingCtrl.create({
      content: 'Loading Offer Details'
    })
    loader.present();
    let itemDetailSubscription = this.dS.getOfferDetail(item.offerId)
      .subscribe(res => {
        console.log('res get single offers', res);
        this.itemDetails = res.data.offer[0];
        this.itemImages = res.data.offer[0].offerImage
        this.isWatchlist = res.data.offer[0].isWatchlist;
        console.log('this.isWatchlist', this.isWatchlist);
        loader.dismiss();
        itemDetailSubscription.unsubscribe()
      })
  }
  ionViewWillEnter() {

  }

  getBackground(image) {
    // console.log("image url:",image)
    // return this._sanitizer.bypassSecurityTrustStyle(`linear-gradient( rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
    // return this._sanitizer.bypassSecurityTrustStyle(`url(${"https://pbs.twimg.com/profile_images/772061312545415169/b78PaZsl.jpg"})`);
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }

  makeOffer() {
    this.navCtrl.push('MakeOfferPage', { itemDetails: this.itemDetails })
  }

  messageSeller() {
    let modal = this.modalCtrl.create('MessageModalPage', { itemDetails: this.itemDetails })
    modal.present()
    modal.onDidDismiss((data) => {
      if (data) {
        console.log(`message ${data.message}`)
        let itemDetails = this.itemDetails;
        this.dS.messageSeller(
          itemDetails.profileId,
          itemDetails.offerId,
          data.message).subscribe(resp => {
            console.log('messge seller resp', resp);
          }, error => {
            console.log(`${error}`)
          })
      }
    })

  }
  getSellerProfile() {
    let loader = this.loadingCtrl.create()
    loader.present();
    if (this.prevPage == 'ProfilePage') {
      this.navCtrl.pop();
      loader.dismiss();
    } else {
      let itemDetails = this.itemDetails;
      this.dS.getSellerProfile(itemDetails.profileId)
        .subscribe(resp => {
          console.log(resp)
          if (resp.status == 'OK') {
            this.navCtrl.push('ProfilePage', { profileData: resp.data })
            loader.dismiss();
          } else {
            console.error('error');
            loader.dismiss();
          }
        }, error => {
          console.error(error);
          loader.dismiss();
        })
    }
  }

  toggleWatchList() {
    this.isWatchlist = this.isWatchlist == true ? false : true;
    this.dS.toggleWatchList(this.itemDetails.offerId).subscribe(resp => {
      console.log('toggle response', resp);
      // this.dS.getOfferDetail(this.itemDetails.offerId)
      // .subscribe(res => {
      //   console.log('item details',res);
      //   this.itemDetails = res.data.offer[0];
      //   this.itemImages = res.data.offer[0].offerImage
      //   this.isWatchlist = res.data.offer[0].isWatchlist;
      // })
    }, error => console.error(error));
  }

  report() {
    let modal = this.modalCtrl.create('ReportPage', { itemDetails: this.itemDetails });
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        console.log(data);
        this.navCtrl.push('ReportTwoPage', { report: data, offerId: this.itemDetails.offerId, itemDetails: this.itemDetails })
      }
    })
  }

  share() {
    let modal = this.modalCtrl.create('ShareModalPage', { title: 'Share Item' });
    modal.present();
  }

}
