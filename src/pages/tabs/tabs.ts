import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs, ModalController, App, Nav, Events } from 'ionic-angular';
import { RegistrationModalPage } from "../registration-modal/registration-modal";
import { Accounts, Alerts, Offers, /* Picture, Details */ } from "../pages";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { DataServiceProvider } from "../../providers/data-service/data-service";
import { Subscription } from "rxjs";
import { TranslateService, /* LangChangeEvent */ } from '@ngx-translate/core';
import { PostingServiceProvider } from "../../providers/posting-service/posting-service";

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  @ViewChild('tabs') tabs: Tabs

  tab1Root: any
  tab2Root: any
  tab3Root: any
  tab4Root: any
  tab5Root: any
  tabIndex = 0;
  seltabix: number = 0
  isLoggedIn: boolean = false;
  private userSubscription: Subscription;
  private modalSubscription: Subscription;
  userId;
  testVaraible: boolean = true;

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public app: App,
    private auth: AuthServiceProvider,
    private dataService: DataServiceProvider,
    private pS: PostingServiceProvider,
    public translate: TranslateService,
    /* public tabs: Tabs */) {

    this.tab1Root = 'HomePage'
    this.tab2Root = Alerts
    this.tab3Root = 'PostPage'
    this.tab4Root = Offers
    this.tab5Root = Accounts

    this.tabs = navCtrl.parent;

  }
  ionViewWillEnter() {
    if (this.pS.offersList && this.pS.offersList.length == 0 || this.pS.newPostAdded == true) {
      console.log('something happend'); 
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

  ionViewCanLeave(): boolean {
    if (this.auth._user) {
      console.log("its all good")
      return true
    } else {
      console.error("Surprize MF")
      return false
    }
  }

  testModal() {
    let modal = this.modalCtrl.create(RegistrationModalPage, { fromTabs: true })
    modal.present()
    modal.onWillDismiss(
      () => {
        this.modalSubscription = this.auth.isLoginSubject
          .subscribe(resp => {
            console.log(resp)
            if (resp == null) {
              this.isLoggedIn = false
              this.app.getRootNav().getActiveChildNav().select(0);
              // userSubscription.unsubscribe()
            } else {
              this.isLoggedIn = true;
              // userSubscription.unsubscribe()
            }
          })
      }
    );
  }

  goToTab() {
    this.userSubscription = this.auth.isLoginSubject
      .subscribe(resp => {
        if (resp) {
          this.isLoggedIn = true;
          this.navCtrl.push('AdPicture');
          // this.userSubscription.unsubscribe()
        } else {
          this.testModal()
          // this.userSubscription.unsubscribe()
        }
      })
  }

  gotToTab1() {
    // this.navCtrl.setRoot(this.tab4Root);
    this.tabs.select(3)
  }

  ionViewWillLeave() {
    console.log("unsubscribing from tabs page");
    this.dataService.sellingDataLoaded = false;
    if (this.userSubscription) this.userSubscription.unsubscribe()
    if (this.modalSubscription) this.modalSubscription.unsubscribe()
  }
}
