import { Component } from '@angular/core';
import { IonicPage , NavController, NavParams } from 'ionic-angular';
import { PostingServiceProvider } from "../../providers/posting-service/posting-service";
import { LocationServiceProvider } from "../../providers/location-service/location-service";


@IonicPage()
@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {
  priceFrom = '0'; 
  priceTo = '0';
  distance: number = 2;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private locationService: LocationServiceProvider,
    private pS: PostingServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilterPage');
  }

  apply() {
    // let filter = this.pS.filters;
    // this.pS.filters.filter = (filter.sortBy == 'newest') ? '3' : '4';
    // this.pS.filters.filter = '3';
    this.pS.offersList = [];
    this.navCtrl.pop()
  }

}
