import { AccountPage } from "./account/account";
import { AlertsPage } from "./alerts/alerts";
import { OffersPage } from "./offers/offers";
import { Signup } from "./signup/signup";
import { Login } from "./login/login";
import { RegistrationModalPage } from "./registration-modal/registration-modal";
// import { AdPicture } from "./ad-picture/ad-picture";
// import { AdDetails } from "./ad-details/ad-details";

export const Alerts = AlertsPage
export const Accounts = AccountPage
export const Offers = OffersPage
export const Registration = RegistrationModalPage
export const LoginPage = Login
export const SignupPage = Signup
// export const Picture = AdPicture
// export const Details = AdDetails