import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RateBuyerPage } from './rate-buyer';
import { Ionic2RatingModule } from "ionic2-rating";

@NgModule({
  declarations: [
    RateBuyerPage,
  ],
  imports: [
    IonicPageModule.forChild(RateBuyerPage),
    Ionic2RatingModule
  ],
  exports: [
    RateBuyerPage
  ]
})
export class RateBuyerPageModule {}
