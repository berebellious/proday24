import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { DataServiceProvider } from "../../providers/data-service/data-service";


@IonicPage()
@Component({
  selector: 'page-rate-buyer',
  templateUrl: 'rate-buyer.html',
})
export class RateBuyerPage {
  profileImage: string;
  rate: number = 0;
  item: any;
  user: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private dS: DataServiceProvider) {

    let user = navParams.get('user');
    this.user = user;
    this.item = navParams.get('item');
    this.profileImage = `url(${user.profileImage})`
    console.log(user);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RateBuyerPage');
  }

  rating(event) {
    console.log(event);
  }

  submitReview() {
    let loader = this.loadingCtrl.create();
    loader.present();
    this.dS.review(this.user.userId, this.rate).subscribe(resp => {
      console.log(resp);
      loader.dismiss().then(() => this.navCtrl.popToRoot());
    })
  }

}
