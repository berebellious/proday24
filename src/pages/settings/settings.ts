import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { UserServiceProvider } from "../../providers/user-service/user-service";
import { AuthServiceProvider, UserInfo } from "../../providers/auth-service/auth-service";
import { Subscription } from "rxjs";


@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  userInfo: UserInfo;
  fieldsDisabled = {
    name: true,
    email: true,
    phone: true
  };
  fields = {
    name: '',
    email: '',
    phone: ''
  }
  private userSubscription: Subscription;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public auth: AuthServiceProvider,
    private userService: UserServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  enableInput(field) {
    this.fieldsDisabled[field] == true ?
      this.fieldsDisabled[field] = false :
      this.fieldsDisabled[field] = true;
  }

  ionViewWillEnter() {
    this.getUserInfo()
  }

  getUserInfo() {
    let loader = this.loadingCtrl.create({
      content: 'Fetching personal data'
    });
    loader.present()
    this.userSubscription = this.auth.isLoggedIn()
    .subscribe(resp => {
      console.log("listening from settings page", resp)
      this.userInfo = resp;
      this.fields['name'] = resp.firstName;
      this.fields['email'] = resp.email;
      this.fields['phone'] = resp.phone;
      loader.dismiss()
    }, error => {
      loader.dismiss()
      console.error(error);
    })
    // this.userService.getUserInfo()
    //   .subscribe(resp => {
    //     console.log(resp)
    //     if (resp.data.message == 'Success') {
    //       this.userInfo = resp.data.profile;
    //       this.fields['name'] = resp.data.profile.first_name;
    //       this.fields['email'] = resp.data.profile.email;
    //       this.fields['phone'] = resp.data.profile.phone;
    //       loader.dismiss()
    //     } else {
    //       loader.dismiss()
    //       throw resp.data.message
    //     }
    //   }, error => console.error(error))
  }

  async save() {
    let loader = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    console.log(this.fields)
    let info = {
      firstName: this.fields.name,
      lastName: this.fields.name,
      email: this.fields.email,
      phone: this.fields.phone
    }
    loader.present()
    const resp = await this.userService.updateInfo(info).toPromise()
    console.log(resp)
    if (resp.data.success == true) {
      const user = await this.auth.updateLoginSubject(info);
      console.log("user from updateLoginSubject", user)
    }
    this.navCtrl.pop()
  }

  ionViewWillLeave() {
    console.log("unsubscribing from settings page")
    if (this.userSubscription) this.userSubscription.unsubscribe()
  }
}
