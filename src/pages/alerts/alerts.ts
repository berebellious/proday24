import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, App, LoadingController } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
// import { DataServiceProvider } from "../../providers/data-service/data-service";
import { ChatServiceProvider } from "../../providers/chat-service/chat-service";
import { Subscription } from "rxjs";
import { RegistrationModalPage } from "../registration-modal/registration-modal";


@Component({
  selector: 'page-alerts',
  templateUrl: 'alerts.html',
})
export class AlertsPage {
  isLoggedIn: boolean = false;
  private userSubscription: Subscription;
  private modalSubscription: Subscription;
  userId;
  messages;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private auth: AuthServiceProvider,
    // private dS: DataServiceProvider,
    private chatService: ChatServiceProvider,
    public app: App,
    public modalCtrl: ModalController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AlertsPage');
  }

  ionViewWillEnter() {
    let loader = this.loadingCtrl.create({
      content: 'Loading Alerts'
    })
    loader.present()
    this.userSubscription = this.auth.isLoginSubject
      .subscribe(resp => {
        console.log("listening from alerts", resp)
        if (resp) {
          this.isLoggedIn = true;
          this.userId = resp.userId
          this.getChatList();
          loader.dismiss();
        } else {
          this.isLoggedIn = false
          this.testModal();
          loader.dismiss();
        }
      })
  }

  testModal() {
    let modal = this.modalCtrl.create(RegistrationModalPage, { fromTabs: true })
    modal.present()
    modal.onWillDismiss(
      () => {
        this.modalSubscription = this.auth.isLoginSubject
          .subscribe(resp => {
            console.log(resp)
            if (resp == null) {
              this.isLoggedIn = false
              this.app.getRootNav().getActiveChildNav().select(0);
              // userSubs.unsubscribe()
            } else {
              this.isLoggedIn = true;
              // userSubs.unsubscribe()
            }
          })
      }
    );
  }

  getChatList() {
    this.chatService.getChatList().subscribe(resp => {
      console.log("The response ", resp)
      if (resp.message == "Data received") {
        this.messages = resp.data.list;
      }
    }, error => {
      console.error(error)
    });
  }

  ionViewWillLeave() {
    console.log("unsubscribing from alerts page")
    if (this.userSubscription) this.userSubscription.unsubscribe()
    if (this.modalSubscription) this.modalSubscription.unsubscribe()
  }

  chat(msg) {
    // this.chatMessage.userImgUrl = msg.image;
    // this.chatMessage.userName = msg.name;
    // this.chatMessage.toUserId = msg.otherId;
    // this.chatMessage.offerId = this.item.offerId;
    // this.chatService.image = msg.image;
    // this.chatService.name = msg.name;
    // this.chatService.toUserId = msg.otherId;
    // this.chatService.itemDetails = this.item;
    this.navCtrl.push('ChatsAlertPage', {
      toUserImage: msg.image,
      toUserName: msg.name,
      toUserId: msg.otherId
    });
  }

}
