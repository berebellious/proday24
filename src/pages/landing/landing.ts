import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { LoginPage, SignupPage } from "../pages";
import { RegistrationModalPage } from "../registration-modal/registration-modal";
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
// import { UserServiceProvider } from "../../providers/user-service/user-service";


@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class Landing {

  success: boolean = true
  failMsg: string = ''
  loggedIn: boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    // private userService: UserServiceProvider,
    private fb: Facebook,
    private authService: AuthServiceProvider,
    public registration: RegistrationModalPage) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Landing');
  }

  goToLogin() {
    // this.navCtrl.push(LoginPage);
    this.registration.loginContent()
  }

  goToSignup() {
    // this.navCtrl.push(SignupPage);
    this.registration.signupContent()
  }

  async facebook() {
    let permission = ['email', 'public_profile'];
    try {
      const fbLoginResp: FacebookLoginResponse = await this.fb.login(permission)
      console.log('resp from FB login', fbLoginResp);
      const graphApiResp = await this.fb.api(`/me?fields=id,name,email,first_name,last_name,picture.width(720).height(720).as(picture_large)`, permission)
      console.log('resp from FB api', graphApiResp);
      let loader = this.loadingCtrl.create();
      loader.present();
      const socaialResp = await this.authService.socialLogin(graphApiResp).toPromise();
      console.log('socaialResp', socaialResp);
      await this.authService._initialiseUser(socaialResp.data, graphApiResp.picture_large.data.url, true);
      loader.dismiss()
      this.registration.fromTabs ? this.registration.cancel() : this.registration.skip()
    } catch (error) {
      console.error('fb error', error);
    }
  }

}
