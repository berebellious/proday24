import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { DataServiceProvider } from "../../providers/providers";


@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class Categories {
  categories = [];
  rows: any;
  movies = [{ title: 'One' }, { title: 'Two' }, { title: 'Three' }, { title: 'Four' }, { title: 'Five' }, { title: 'Six' }]
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public ds: DataServiceProvider) {

    this.getCategories();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Categories');
  }

  ionViewWillEnter() {
    // this.getCategories();
    // this.categories = this.ds.getCategories()
  }

  getCategories() {
    let loader = this.loadingCtrl.create()
    loader.present()
    this.ds.getCategories()
      .subscribe(res => {
        loader.dismiss()
        console.log(res);
        this.categories = res.data.categories;
        console.log(this.categories.length)
        let length = this.categories.length
        // this.rows = Array.from(Array(Math.ceil(res.data.categories.length / 3))).keys()
        this.rows = Array.from(Array(Math.ceil(length / 2)).keys())
        console.log("rows", this.rows)
      }, error => {
        console.error(error)
        loader.dismiss()
      })
  }

  getCategoryOffers(category) {
    this.navCtrl.push('CategoryOffersPage', { category: category })
  }

}
