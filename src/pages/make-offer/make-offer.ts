import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { DataServiceProvider } from "../../providers/data-service/data-service";


@IonicPage()
@Component({
  selector: 'page-make-offer',
  templateUrl: 'make-offer.html',
})
export class MakeOfferPage {
  itemDetails;
  inputDisabled: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private dataService: DataServiceProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController) {

    this.itemDetails = navParams.get('itemDetails')
    console.log(this.itemDetails)
    if (this.itemDetails.firmPrice == "Yes" || this.itemDetails.firmPrice == "yes") {
      this.inputDisabled = true
    } else {
      this.inputDisabled = false
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MakeOfferPage');
  }

  makeOffer() {
    let loader = this.loadingCtrl.create({
      content: "Bidding"
    });
    loader.present()
    this.dataService.makeOffer(
      this.itemDetails.offerId,
      this.itemDetails.profileId,
      this.itemDetails.price)
      .subscribe(resp => {
        console.log(resp)
        loader.dismiss()
        if (resp.status == "OK") {
          let alert = this.alertCtrl.create({
            title: 'Success',
            subTitle: resp.data.message,
            buttons: ['OK']
          });
          alert.present()
        }
      }, error => {
        console.error("error making offer", error)
      })
  }

}
