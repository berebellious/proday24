import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ViewController } from 'ionic-angular';
import { LocationServiceProvider } from "../../providers/location-service/location-service";


@IonicPage()
@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage {

  public latitude;
  public longitude;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public viewCtrl: ViewController,
    private locationService: LocationServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LocationPage');
  }

  /* async setLocation() {
    let location = await this.locationService.getCurrentLocation()
    this.latitude = location.latitude
    this.longitude = location.longitude
  } */

  async setLocation() {
    let loader = this.loadingCtrl.create();
    loader.present();
    try {
      await this.locationService.setNewLocation();
      loader.dismiss();
    } catch (error) {
      console.error('error setting new location', error);
      loader.dismiss();
    }
  }

  async getPostalCodeLocation() {
    let loader = this.loadingCtrl.create();
    loader.present();
    try {
      let geoResults = await this.locationService.geocode().toPromise();
      console.log(geoResults);
      console.log(geoResults.results[0]);
      let lat = geoResults.results[0].geometry.location.lat;
      let lng = geoResults.results[0].geometry.location.lng;
      let address = geoResults.results[0].formatted_address;
      let postalCode = this.locationService.postalCode;
      await this.locationService.setPostalLocation(lat, lng, postalCode, address);
      loader.dismiss();
    } catch (error) {
      console.error('error in geocode', error);
      loader.dismiss();
    }
  }

  save() {
    this.viewCtrl.dismiss('locationUpdated');
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

}
