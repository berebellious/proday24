import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { Content, TextInput, LoadingController } from 'ionic-angular';

import { ChatServiceProvider, ChatMessage, ToUserInfo } from "../../providers/chat-service/chat-service";
import { UserServiceProvider } from "../../providers/user-service/user-service";
import { Observable, Subscription } from "rxjs";

@IonicPage()
@Component({
    selector: 'page-chat',
    templateUrl: 'chat.html',
})
export class Chat {

    @ViewChild(Content) content: Content;
    @ViewChild('chat_input') messageInput: TextInput;
    msgList: ChatMessage[] = [];
    userId: string;
    userName: string;
    userImgUrl: string;
    toUserId: string;
    toUserName: string;
    editorMsg: string = '';
    _isOpenEmojiPicker = false;
    offerId: any;
    polling: Subscription;

    constructor(public navParams: NavParams,
        public chatService: ChatServiceProvider,
        public toUserInfo: ToUserInfo,
        public loadingCtrl: LoadingController,
        public userService: UserServiceProvider) {

        // Get the navParams toUserId parameter
        this.toUserInfo.userId = navParams.get('toUserId');
        this.toUserInfo.userName = navParams.get('toUserName');
        this.toUserInfo.userImgUrl = navParams.get('toUserImage');
        this.offerId = navParams.get('offerId');
        // Get mock user information
        this.userService.getUserInfo()
            .subscribe((resp) => {
                this.userId = resp.data.profile.user_id;
                this.userName = resp.data.profile.first_name;
                this.userImgUrl = resp.data.profile.image;
            });
    }

    ionViewDidLoad() {
        // this.switchEmojiPicker();

    }

    ionViewWillLeave() {
        // unsubscribe
        this.polling.unsubscribe();
        this.toUserInfo.userId = ''
        this.toUserInfo.userName = ''
        this.toUserInfo.userImgUrl = ''
        this.offerId = ''

    }

    ionViewWillEnter() {
        //get message list
        let loader = this.loadingCtrl.create({
            content: 'fetching messages'
        });
        loader.present();
        this.getMsg()
            .then(() => {
                console.log(this.msgList);
                this.scrollToBottom();
                loader.dismiss();
            });

        // Subscribe to received  new message events
        // this.chatService.mockNewMsg().subscribe
        this.polling = Observable.interval(5000)
            .switchMap(() => this.chatService.mockNewMsg(this.offerId))
            .subscribe(resp => {
                console.log("polling data", resp);
                if (resp.message == "Data received") {
                    resp.data.list.map(msg => {
                        resp.data.list.map((msg: ChatMessage) => {
                            this.pushNewMsg(msg);
                        })
                    })
                }
            })
        // this.chatService.mockNewMsg().subscribe(data => console.log("polling data", data))
    }

    _focus() {
        this._isOpenEmojiPicker = false;
        this.content.resize();
        this.scrollToBottom();
    }

    switchEmojiPicker() {
        this._isOpenEmojiPicker = !this._isOpenEmojiPicker;
        if (!this._isOpenEmojiPicker) {
            this.messageInput.setFocus();
        }
        this.content.resize();
        this.scrollToBottom();
    }

    /**
     * @name getMsg
     * @returns {Promise<ChatMessage[]>}
     */
    async getMsg() {
        // Get mock message list
        return this.chatService
            .getMsgList(this.offerId)
            .then(res => {
                console.log(res);
                this.msgList = res.data.list;
                console.log(this.msgList)
            })
            .catch(err => {
                console.log(err)
            })
    }

    /**
     * @name sendMsg
     */
    sendMsg() {

        if (!this.editorMsg.trim()) return;

        // Mock message
        // const id = Date.now().toString();
        let newMsg: ChatMessage = {
            mtime: Date.now(),
            message: this.editorMsg,
            side: 'right'
        };

        this.pushNewMsg(newMsg);
        this.editorMsg = '';

        if (!this._isOpenEmojiPicker) {
            this.messageInput.setFocus();
        }

        this.chatService.sendMsg(newMsg, this.offerId)
            .then((data) => {
                console.log('send msg:', data);
                // let index = this.getMsgIndexById(id);
                // if (index !== -1) {
                //     this.msgList[index].status = 'success';
                // }
            })
    }

    /**
     * @name pushNewMsg
     * @param msg
     */
    pushNewMsg(msg) {
        // Verify user relationships
        // if (msg.userId === this.userId && msg.toUserId === this.toUserId) {
        //     this.msgList.push(msg);
        // } else if (msg.toUserId === this.userId && msg.userId === this.toUserId) {
        //     this.msgList.push(msg);
        // }
        console.log('pushing msg');
        this.msgList.push(msg);
        this.scrollToBottom();
    }

    // getMsgIndexById(id: string) {
    //     return this.msgList.findIndex(e => e.messageId === id)
    // }

    scrollToBottom() {
        setTimeout(() => {
            if (this.content.scrollToBottom) {
                this.content.scrollToBottom();
            }
        }, 400)
    }
}
