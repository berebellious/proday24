import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Chat } from './chat';
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [
    Chat,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(Chat),
  ],
  exports: [
    Chat
  ]
})
export class ChatPageModule {}
