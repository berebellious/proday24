import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdPicture } from './ad-picture';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AdPicture,
  ],
  imports: [
    IonicPageModule.forChild(AdPicture),
    TranslateModule.forChild()
  ],
  exports: [
    AdPicture
  ]
})
export class AdPictureModule {}
