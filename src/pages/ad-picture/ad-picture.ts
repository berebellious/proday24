import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
// import { Storage } from "@ionic/storage";
import { File } from "@ionic-native/file";
import { PostingServiceProvider } from "../../providers/posting-service/posting-service";
import { CameraServiceProvider } from "../../providers/camera-service/camera-service";
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { PhotoViewer } from "@ionic-native/photo-viewer";

@IonicPage()
@Component({
  selector: 'page-ad-picture',
  templateUrl: 'ad-picture.html',
})
export class AdPicture {
  langs = ['en', 'rus'];
  emptyTitle: boolean = false
  showButton: boolean = false;
  showIcon: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private cameraService: CameraServiceProvider,
    private photoViewer: PhotoViewer,
    // private storage: Storage,
    public postService: PostingServiceProvider,
    public translate: TranslateService,
    private file: File) {

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      console.log('Language changed to ' + this.translate.currentLang);
    });

    if (postService.images.length == 0) {
      this.showButton = true
    } else this.showButton = false

    if (postService.images.length > 0 && postService.images.length < 4) {
      this.showIcon = true
    } else this.showIcon = false
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdPicture');
  }

  async takeAPicture() {
    try {
      let imageObject = await this.cameraService.takeAPicture();
      console.log("imageObject", imageObject);
      this.postService.images.push(imageObject)
      console.log(this.postService.images);
    } catch (error) {
      console.error(`${error}`)
    }
  }

  async chooseFromGallery() {
    try {
      let imageObject = await this.cameraService.chooseFromGallery();
      console.log("imageObject", imageObject);
      this.postService.images.push(imageObject)
      console.log(this.postService.images);
    } catch (error) {
      // console.error("error in ad-picture", error)
      console.error(`${error}`)
    }
  }

  viewPhoto(uri) {
    this.photoViewer.show(uri);
  }


  takePhoto() {
    let alert = this.alertCtrl.create({
      title: 'Take a photo',
      buttons: [
        {
          text: 'Take A Picture',
          handler: () => {
            this.takeAPicture()
          }
        },
        {
          text: 'Choose From Gallery',
          handler: () => {
            this.chooseFromGallery()
          }
        }
      ]
    });
    alert.present()
  }

  next() {
    console.log("next clicked")
    if (this.postService.title == '') {
      this.emptyTitle = true
    } else this.emptyTitle = false
    if (this.postService.images.length == 0) this.errorToast()
    this.navCtrl.push('AdDetails')
  }

  errorToast() {
    let toast = this.toastCtrl.create({
      message: "Attach a picture of the item",
      duration: 3000
    });
    toast.present()
  }

  picupload() {
    this.postService.picupload()
  }

  remove() {
    if (this.postService.images.length >= 1) {
      this.postService.images.splice(0, 1);
    }
  }

}
