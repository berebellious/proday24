import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoryListingPage } from './category-listing';

@NgModule({
  declarations: [
    CategoryListingPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoryListingPage),
  ],
  exports: [
    CategoryListingPage
  ]
})
export class CategoryListingPageModule {}
