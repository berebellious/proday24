import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DataServiceProvider } from "../../providers/providers";
import { PostingServiceProvider } from "../../providers/posting-service/posting-service";

@IonicPage()
@Component({
  selector: 'page-category-listing',
  templateUrl: 'category-listing.html',
})
export class CategoryListingPage {
  categories;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private ds: DataServiceProvider,
    private postService: PostingServiceProvider) {

    this.getCategories();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryListingPage');
  }

  getCategories() {
    // this.categories = this.ds.getCategories();
    this.ds.getCategories().subscribe(res => {
      console.log(res);
      this.categories = res.data.categories;
      console.log(this.categories.length)
      // let length = this.categories.length
    })
  }

  selectCategory(category, categoryId) {
    this.postService.selectedCategory.category = category;
    this.postService.selectedCategory.categoryId = categoryId;
    this.navCtrl.pop()
    // this.viewCtrl.dismiss({ selectCategory: category })
  }
}
