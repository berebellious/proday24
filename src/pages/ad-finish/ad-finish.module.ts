import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdFinishPage } from './ad-finish';

@NgModule({
  declarations: [
    AdFinishPage,
  ],
  imports: [
    IonicPageModule.forChild(AdFinishPage),
  ],
  exports: [
    AdFinishPage
  ]
})
export class AdFinishPageModule {}
