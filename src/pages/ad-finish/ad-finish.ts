import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import { LocationServiceProvider } from "../../providers/location-service/location-service";
import { PostingServiceProvider } from "../../providers/posting-service/posting-service";

@IonicPage()
@Component({
  selector: 'page-ad-finish',
  templateUrl: 'ad-finish.html',
})
export class AdFinishPage {

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public locationService: LocationServiceProvider,
    public loadingCtrl: LoadingController,
    public postingservice: PostingServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdFinishPage');
  }

  async postItem() {
    let loader = this.loadingCtrl.create({
      content: 'posting your ad',
      dismissOnPageChange: true
    });
    loader.present()
    this.postingservice
      .newPost(this.locationService.latitude,
      this.locationService.longitude)
        .subscribe(async resp => {
          console.log(resp);
          this.postingservice.offerId = resp.data.offerId;
          await this.postingservice.picupload();
          this.postingservice.clearData();
          this.postingservice.newPostAdded = true;
          this.events.publish('newPostAdded');
          this.navCtrl.popToRoot()
        }
      )
  }
}
