import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataServiceProvider } from "../../providers/data-service/data-service";

@IonicPage()
@Component({
  selector: 'page-boards',
  templateUrl: 'boards.html',
})
export class BoardsPage {

  boardList = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public dS: DataServiceProvider) {
  }

  ionViewWillEnter() {
    this.loadBoards();
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad BoardsPage');
  }

  loadBoards() {
    this.dS.getWatchList().subscribe(resp => {
      console.log(resp);
      this.boardList = resp.data.offersList;
    })
  }

}
