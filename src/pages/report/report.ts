import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DataServiceProvider } from "../../providers/data-service/data-service";


@IonicPage()
@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {

  reportText = ["It's prohibited on Proday",
    "It's offesnsive to me",
    "It's not a real post",
    "It's not a duplicate post",
    "It's in the wrong category",
    "It may be scam",
    "It may be stolen",
    "Other"
  ]
  itemDetails: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private dS: DataServiceProvider) {

    this.itemDetails = navParams.get('itemDetails')
    console.log(this.itemDetails);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportPage');
  }

  report(report: string) {
    console.log(report);
    this.viewCtrl.dismiss(report);
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

}
