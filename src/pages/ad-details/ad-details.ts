import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { AdPicture } from "../ad-picture/ad-picture";
import { PostingServiceProvider } from "../../providers/posting-service/posting-service";


@IonicPage()
@Component({
  selector: 'page-ad-details',
  templateUrl: 'ad-details.html',
})
export class AdDetails {
  // adPicture: AdPicture;
  public condition: number = this.postService.condition;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public postService:  PostingServiceProvider
     /* public adPicture: AdPicture */ ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdDetails');
  }

  ionViewCanEnter(): boolean {
    console.log("this.postService.title", this.postService.title)
    if (this.postService.title == '' || this.postService.images.length <= 0) {
      return false;
    } else return true
  }

  categories() {
    this.navCtrl.push('CategoryListingPage')
  }

  testRange() {
    console.log(this.postService.condition)
  }
}
