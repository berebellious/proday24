import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdDetails } from './ad-details';

@NgModule({
  declarations: [
    AdDetails,
  ],
  imports: [
    IonicPageModule.forChild(AdDetails),
  ],
  exports: [
    AdDetails
  ]
})
export class AdDetailsModule {}
