import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/providers";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { EmailValidator } from "../../validators/email";
import { RegistrationModalPage } from "../registration-modal/registration-modal";
// import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook";

// @IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
  @ViewChild('input') myInput: ElementRef

  loginForm: FormGroup
  submitAttempt: boolean = false
  success: boolean = true
  failMsg: string = ''
  public loggedIn: boolean = false

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private auth: AuthServiceProvider,
    // private fb: Facebook,
    private registration: RegistrationModalPage,
    public formBuilder: FormBuilder) {

    
    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    })
    // this.myInput._jsSetFocus()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
    this.myInput.nativeElement.setFocus()
  }

  lsCancel() {
    this.loginForm.reset()
    this.registration.lsCancel()
    this.submitAttempt = false
  }

  signup() {
    // this.registration.signupContent()
    this.registration.landingContent()
  }

  login() {
    let loader = this.loadingCtrl.create()
    this.submitAttempt = true
    if (!this.loginForm.valid) {
    } else {
      loader.present()
      let email = this.loginForm.value.email
      let pwd = this.loginForm.value.password

      this.auth.login(email, pwd)
        .subscribe(async resp => {
          if (resp && resp.data.success) {
            this.loggedIn = true
            await this.auth._initialiseUser(resp.data, null, false)
            loader.dismiss()
            this.registration.fromTabs ? this.registration.cancel() : this.registration.skip()
            // this.viewCtrl.dismiss()
          } else {
            loader.dismiss()
            this.success = false
            this.failMsg = resp.data.message
            console.error(resp);
            let alert = this.alertCtrl.create({
              title: "Error",
              subTitle: resp.data.message,
              buttons: ["OK"]
            });
            alert.present()
          }
        }, error => {
          console.error(error);
        })
    }
  }

  cancel() {
    this.viewCtrl.dismiss({ isLoggedIn: false })
  }
}
