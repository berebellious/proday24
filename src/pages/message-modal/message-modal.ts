import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-message-modal',
  templateUrl: 'message-modal.html',
})
export class MessageModalPage {

  private message: string = '';
  itemDetails: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController) {

    this.itemDetails = navParams.get('itemDetails')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessageModalPage');
  }

  cancel() {
    this.viewCtrl.dismiss()
  }

  send() {
    this.viewCtrl.dismiss({
      message: this.message
    })
  }

}
