import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataServiceProvider } from "../../providers/data-service/data-service";
import { ChatServiceProvider } from "../../providers/chat-service/chat-service";
import { Subscription } from "rxjs";


@IonicPage()
@Component({
  selector: 'page-selling-detail',
  templateUrl: 'selling-detail.html',
})
export class SellingDetailPage {
  item: any;
  itemDetailSubscription: Subscription;
  offersMessages = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private chatService: ChatServiceProvider,
    private dataService: DataServiceProvider) {

    this.item = this.navParams.get('itemDetails')
    console.log(this.item);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SellingDetailPage');
  }

  async ionViewWillEnter() {
    this.chatService.getOfferChatList(this.item.offerId).subscribe(resp => {
      console.log("resp", resp);
      if (resp.message == "Data received") {
        this.offersMessages = resp.data.list;
      }
    }, error => {
      console.error('error', error);
    })
  }

  goToItemDetail(item) {
    this.navCtrl.push('ItemDetailPage', { item: item, fromSelling: true })
    // this.itemDetailSubscription = this.dataService.getOfferDetail(itemDetails.offerId)
    //   .subscribe(res => {
    //     console.log(res)
    //     this.itemDetailSubscription.unsubscribe()
    //   })
  }

  chat(msg) {
    this.navCtrl.push('Chat', {
      toUserImage: msg.image,
      toUserName: msg.name,
      toUserId: msg.otherId,
      offerId: this.item.offerId
    });
  }

  markAsSold(item) {
    this.navCtrl.push('MarkSoldPage', { offerId: this.item.offerId, item: item });
  }

}
