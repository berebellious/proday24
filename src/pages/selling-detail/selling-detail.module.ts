import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SellingDetailPage } from './selling-detail';

@NgModule({
  declarations: [
    SellingDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SellingDetailPage),
  ],
  exports: [
    SellingDetailPage
  ]
})
export class SellingDetailPageModule {}
