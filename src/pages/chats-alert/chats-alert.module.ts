import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatsAlertPage } from './chats-alert';
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [
    ChatsAlertPage,
  ],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(ChatsAlertPage),
  ],
  exports: [
    ChatsAlertPage
  ]
})
export class ChatsAlertPageModule {}
