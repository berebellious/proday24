import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { DataServiceProvider } from "../../providers/data-service/data-service";
import { Subscription } from "rxjs";

@IonicPage()
@Component({
  selector: 'page-buying',
  templateUrl: 'buying.html',
})
export class BuyingPage {
  
  public buyingOffers: Subscription
  public offersList = []

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private dataService: DataServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuyingPage');
  }

  ionViewWillEnter() {
    let loader = this.loadingCtrl.create({
      content: "Loading Offers"
    });
    loader.present();
    // this.dataService.buyingData ? '' : loader.present();
    this.buyingOffers = this.dataService.myOffersBuying()
      .subscribe(resp => {
        console.log(resp)
        if (resp.status !== "OK") throw `server error ${resp.code}`
        this.offersList = resp.data.offersList
        loader.dismiss()
      }, error => {
        console.error(`${error}`)
        loader.dismiss()
      })
  }

  ionViewWillLeave() {
    console.log("unsubscribing from selling offers");
    this.buyingOffers.unsubscribe()
  }

}
