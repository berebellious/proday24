import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, Events } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { LocationServiceProvider } from "../../providers/location-service/location-service";
import { DataServiceProvider } from "../../providers/data-service/data-service";
import { SearchServiceProvider } from "../../providers/search-service/search-service";
import { TranslateService } from '@ngx-translate/core';
import { Subscription, Observable } from "rxjs";
import { PostingServiceProvider } from "../../providers/posting-service/posting-service";
import { FormControl } from "@angular/forms";
import { ModalCmp } from 'ionic-angular/components/modal/modal-component';
import { RegistrationModalPage } from "../registration-modal/registration-modal";
import { TabsPage } from "../tabs/tabs";

@IonicPage()
@Component({
  selector: 'page-home-page',
  templateUrl: 'home-page.html',
})
export class HomePage {
  offerSubscription: Subscription;
  offersList = [];
  itemDetailSubscription: Subscription;
  items: Observable<Array<string>>;
  term = new FormControl();
  noOffersYet: boolean;

  constructor(
    public events: Events,
    private tabs: TabsPage,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private auth: AuthServiceProvider,
    private locationService: LocationServiceProvider,
    private pS: PostingServiceProvider,
    private searchService: SearchServiceProvider,
    public translate: TranslateService,
    private dataService: DataServiceProvider) {

    this.items = searchService.search(this.term.valueChanges);
    this.items.subscribe(resp => console.log(resp));
    this.events.subscribe('newPostAdded', () => this.loadOffers());
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  filter() {
    this.navCtrl.push('FilterPage');

  }
  goToCategories() {
    this.navCtrl.push('Categories');
  }

  goToItemDetail(item) {
    let authSubscription: Subscription = this.auth.isLoggedIn()
      .subscribe(resp => {
        if (resp) {
          console.log(item);
          this.navCtrl.push('ItemDetailPage', { item: item });
          authSubscription.unsubscribe();
        } else {
          this.testModal(item);
          authSubscription.unsubscribe();
        }
      })
  }

  testModal(item) {
    let modal = this.modalCtrl.create(RegistrationModalPage, { fromTabs: true })
    modal.present()
    modal.onWillDismiss(
      () => {
        let authSubscription: Subscription = this.auth.isLoggedIn()
          .subscribe(resp => {
            console.log(resp)
            if (resp == null) {
              authSubscription.unsubscribe();
            } else {
              this.navCtrl.push('ItemDetailPage', { item: item });
              authSubscription.unsubscribe();
            }
          })
      }
    );
  }

  setLocation() {
    let locationModal = this.modalCtrl.create('LocationPage');
    locationModal.present();
    locationModal.onWillDismiss(data => {
      if (data) {
        let loader = this.loadingCtrl.create({
          content: 'Loading Offers'
        })
        loader.present();
        try {
          this.loadOffers();
          loader.dismiss();
        } catch (error) {
          loader.dismiss();
        }
      }
    })
  }

  ionViewWillEnter() {
    console.log(`this.tabs.testVaraible ${this.tabs.testVaraible}`);
    if (this.pS.offersList && this.pS.offersList.length == 0) {
      let loader = this.loadingCtrl.create({
        content: 'Loading Offers'
      })
      loader.present();
      try {
        this.loadOffers();
        loader.dismiss();
      } catch (error) {
        loader.dismiss();
      }
    }
  }

  loadOffers() {

    this.offerSubscription = this.dataService
      .getOffers('',
      this.pS.filters.filter,
      this.pS.filters.radius,
      this.pS.filters.firmPrice,
      this.pS.filters.featured,
      this.pS.filters.priceFrom,
      this.pS.filters.priceTo,
    )
      // .getOffers('', 1, 100, 250)
      .subscribe(
      resp => {
        if (resp.data.isEmpty == true) {
          this.noOffersYet = true;
          throw this.noOffersYet;
          // loader.dismissAll();
        } else {
          this.noOffersYet = false;
          let post = this.pS
          let filters = this.pS.filters;
          console.log('resp from home-page', resp)
          if (filters.sortBy === 'newest') {
            post.offersList = resp.data.offersList;
          } else if (post.filters.sortBy === 'LtoH') {
            post.offersList = resp.data.offersList.sort((a, b) => a.price - b.price)
          } else if (post.filters.sortBy === 'HtoL') {
            post.offersList = resp.data.offersList.sort((a, b) => b.price - a.price)
          } else if (post.filters.sortBy === 'closest') {
            post.offersList = resp.data.offersList.sort((a, b) => a.distance - b.distance)
          }

          // post.offersList = (post.filters.sortBy !== 'newest') ?
          //   ((post.filters.sortBy == 'LtoH') ?
          //     resp.data.offersList.sort((a, b) => a.price - b.price) :
          //     resp.data.offersList.sort((a, b) => b.price - a.price)) :
          //   resp.data.offersList;

          console.log('after sort', post.offersList);
          // loader.dismissAll();
        }
      }, error => {
        console.error("getOffers error", error)
        // loader.dismissAll();
        throw error;
      }
      )
  }
  // ionViewCanLeave(): boolean {
  //   if (this.auth._user) {
  //     console.log("its all good")
  //     return true
  //   } else {
  //     console.error("Surprize MF")
  //     return false
  //   }
  // }

  ionViewWillLeave() {
    console.log('unsubscribing from offers')
    this.offerSubscription.unsubscribe()
  }
}
