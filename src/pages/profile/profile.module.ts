import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from "../../components/components.module";
import { ProfilePage } from './profile';
import { Ionic2RatingModule } from "ionic2-rating";

@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ProfilePage),
    Ionic2RatingModule
  ],
  exports: [
    ProfilePage
  ]
})
export class ProfilePageModule {}
