import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { DataServiceProvider } from "../../providers/data-service/data-service";


@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  profileData: any;
  image: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private dS: DataServiceProvider) {

    this.profileData = navParams.get('profileData');
    this.image = `url(${this.profileData.profileInfo.image})`
    console.log(this.profileData);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  follow() {
    this.dS.follow(this.profileData.profileInfo.userId)
      .subscribe(resp => {
        if (resp.data.success == true) {
          let alert = this.alertCtrl.create({
            title: resp.data.message,
            buttons: ['OK']
          });
          alert.present()
          alert.onDidDismiss(() => {this.getSellerProfile()})
        } else {
          let alert = this.alertCtrl.create({
            title: resp.data.message,
            buttons: ['OK']
          });
          alert.present()
        }
      })
  }

  getSellerProfile() {
    this.dS.getSellerProfile(this.profileData.profileInfo.userId)
      .subscribe(resp => {
        console.log(resp)
        if (resp.status == 'OK') {
          this.profileData = resp.data;
        } else {
          console.error('error');
        }
      }, error => {
        console.error(error);
      })
  }
  goToItemDetail(item) {
    console.log(item)
    this.navCtrl.push('ItemDetailPage', { item: item, prevPage: 'ProfilePage' })
  }
}
