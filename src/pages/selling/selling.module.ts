import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from "../../components/components.module";
import { SellingPage } from './selling';

@NgModule({
  declarations: [
    SellingPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(SellingPage),
  ],
  exports: [
    SellingPage
  ]
})
export class SellingPageModule {}
