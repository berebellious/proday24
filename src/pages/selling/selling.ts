import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, App, ModalController } from 'ionic-angular';
import { DataServiceProvider } from "../../providers/data-service/data-service";
import { Subscription } from "rxjs";

@IonicPage()
@Component({
  selector: 'page-selling',
  templateUrl: 'selling.html',
})
export class SellingPage {

  sellingOffers: Subscription;
  offersList = [];
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public appCtrl: App,
    private dataService: DataServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SellingPage');
  }

  loadOffers() {
    let loader = this.loadingCtrl.create({
      content: "Loading Offers"
    });
    loader.present();
    this.sellingOffers = this.dataService.myOffersSelling()
      .subscribe(resp => {
        console.log(resp)
        if (resp.status !== "OK") throw `server error ${resp.code}`
        this.offersList = resp.data.offersList
        this.dataService.sellingDataLoaded = true;
        loader.dismiss();
      }, error => {
        console.error(`${error}`)
        this.dataService.sellingDataLoaded = false;
        loader.dismiss()
      })
  }
  ionViewWillEnter() {
    console.log('entered in selling');
    if (this.dataService.sellingDataLoaded == false) {
      this.loadOffers();
    }
  }

  ionViewWillLeave() {
    console.log("unsubscribing from selling offers");
    this.sellingOffers.unsubscribe();
  }

  goToDetails(itemDetails) {
    this.sellingOffers.unsubscribe();
    this.appCtrl.getRootNav().push('SellingDetailPage', { itemDetails: itemDetails });
  }
  bump(offerImage, offerId) {
    let modal = this.modalCtrl.create('PaymentPage',
      {
        offerImage: offerImage,
        offerId: offerId
      });
    modal.present();
    modal.onWillDismiss((data) => {
      if (data == 'success') {
        this.loadOffers();
      }
    })
  }

}
