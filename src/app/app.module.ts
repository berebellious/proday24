import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Http, HttpModule } from "@angular/http";
import { IonicStorageModule } from "@ionic/storage";
import { Camera } from "@ionic-native/camera";
import { File } from "@ionic-native/file";
import { FileTransfer } from "@ionic-native/file-transfer";
import { FilePath } from "@ionic-native/file-path";
import { Geolocation } from "@ionic-native/geolocation";
import { Stripe } from "@ionic-native/stripe";
import { OneSignal } from "@ionic-native/onesignal";
import { Facebook } from "@ionic-native/facebook";
import { ImageResizer } from "@ionic-native/image-resizer";
import { PhotoViewer } from "@ionic-native/photo-viewer";
// import { SuperTabsModule } from "ionic2-super-tabs";
import { SuperTabsModule } from "../ionic2-super-tabs/src";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { ApiProvider } from '../providers/api/api';
import { DataServiceProvider } from '../providers/data-service/data-service';
import { EmailValidator } from "../validators/email";
import { Login } from "../pages/login/login";
import { RegistrationModalPage } from "../pages/registration-modal/registration-modal";
import { Signup } from "../pages/signup/signup";
import { RegistrationPage } from "../pages/registration/registration";
import { AccountPage } from "../pages/account/account";
import { AlertsPage } from "../pages/alerts/alerts";
import { OffersPage } from "../pages/offers/offers";
import { Landing } from "../pages/landing/landing";
// import { AdPicture } from "../pages/ad-picture/ad-picture";
// import { AdDetails } from "../pages/ad-details/ad-details";
import { PostingServiceProvider } from '../providers/posting-service/posting-service';
import { CameraServiceProvider } from '../providers/camera-service/camera-service';
import { LocationServiceProvider } from '../providers/location-service/location-service';
import { UserServiceProvider } from '../providers/user-service/user-service';
import { ChatServiceProvider, ToUserInfo } from '../providers/chat-service/chat-service';
import { EmojiProvider } from '../providers/emoji/emoji';
import { PipesModule } from "../pipes/pipes.module";
import { ComponentsModule } from "../components/components.module";
import { Ionic2RatingModule } from "ionic2-rating";
import { SearchServiceProvider } from '../providers/search-service/search-service';
import { OneSignalServiceProvider } from '../providers/one-signal-service/one-signal-service';
import { IonicImageLoader } from 'ionic-image-loader';

export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    Login,
    RegistrationModalPage,
    Signup,
    RegistrationPage,
    AccountPage,
    AlertsPage,
    OffersPage,
    Landing,
    // AdPicture,
    // AdDetails
  ],
  imports: [
    BrowserModule,
    HttpModule,
    PipesModule,
    ComponentsModule,
    Ionic2RatingModule,
    IonicModule.forRoot(MyApp, { tabsHideOnSubPages: true }),
    IonicImageLoader.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [Http]
      }
    }),
    SuperTabsModule.forRoot(),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Login,
    RegistrationModalPage,
    Signup,
    RegistrationPage,
    AccountPage,
    AlertsPage,
    OffersPage,
    Landing,
    // AdPicture,
    // AdDetails
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthServiceProvider,
    ApiProvider,
    DataServiceProvider,
    EmailValidator,
    Camera,
    Stripe,
    OneSignal,
    Facebook,
    Geolocation,
    File,
    ImageResizer,
    PhotoViewer,
    FilePath,
    FileTransfer,
    PostingServiceProvider,
    CameraServiceProvider,
    LocationServiceProvider,
    UserServiceProvider,
    ChatServiceProvider,
    EmojiProvider,
    ToUserInfo,
    SearchServiceProvider,
    OneSignalServiceProvider
  ]
})
export class AppModule { }
