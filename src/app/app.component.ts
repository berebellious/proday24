import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthServiceProvider } from "../providers/providers";
import { Subscription } from "rxjs/Subscription";
import { Registration } from "../pages/pages";
import { LocationServiceProvider } from "../providers/location-service/location-service";
import { OneSignalServiceProvider } from "../providers/one-signal-service/one-signal-service";
import { TranslateService } from '@ngx-translate/core';
import { Storage } from "@ionic/storage";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  currentUser;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private oneSignalProvider: OneSignalServiceProvider,
    private auth: AuthServiceProvider,
    private translate: TranslateService,
    private storage: Storage,
    private locationService: LocationServiceProvider) {

    this.initTranslate();

    const authSubscription: Subscription = auth.getAuthToken()
      .subscribe(res => {
        auth._setAuthToken(res.data.authToken);
        console.log("AuthToken", auth.authToken);
        authSubscription.unsubscribe();
        this.isLoggedIn()
      }, error => console.error('getAuthToken error', error));
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.oneSignalProvider.initialiseOneSignal();
      statusBar.styleDefault();

      splashScreen.hide();
    });
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');


    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('en'); // Set your language here
    }

    // this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
    //   this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    // });
  }


  async isLoggedIn() {
    const user = await this.auth.hasToken()
    console.log("user", user);
    if (user) {
      console.log("this.auth.isLoginSubject.value", this.auth.isLoginSubject.value);
      this.locationService.getCurrentLocation();
      this.rootPage = 'TabsPage';
    } else {
      try {
        this.locationService.getCurrentLocation();
        this.rootPage = Registration;
      } catch (error) {
        console.error(error);
      }
    }
  }
}
