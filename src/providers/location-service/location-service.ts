import { Injectable } from '@angular/core';
import { Geolocation, GeolocationOptions } from "@ionic-native/geolocation";
import { Http, URLSearchParams, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/share';
import { Observable } from "rxjs";
import { ApiProvider } from "../api/api";
import { Storage } from "@ionic/storage";
import { locale } from 'moment';
// import { Promise } from 'q';

// declare var google;

@Injectable()
export class LocationServiceProvider {
  public latitude: number;
  public longitude: number;
  public address: string = '';
  public postalCode: string = '';

  constructor(
    private http: Http,
    private geolocation: Geolocation,
    private storage: Storage,
    private api: ApiProvider) {
    console.log('Hello LocationServiceProvider Provider');
  }

  async getCurrentLocation() {
    let options: GeolocationOptions = {
      enableHighAccuracy: true,
      timeout: 10000,
      maximumAge: 100000
    }
    try {
      let userLocation = await this.storage.get('userLocation');
      console.log('userLocation', userLocation);
      if (userLocation !== null) {
        this.latitude = userLocation.lat;
        this.longitude = userLocation.lng;
        this.address = userLocation.address;
        this.postalCode = userLocation.postalCode;
      } else {
        let location = await this.geolocation.getCurrentPosition(options);
        console.log('location', location);
        this.latitude = location.coords.latitude;
        this.longitude = location.coords.longitude;
        let codes = await this.generatePostalCodes(location.coords.latitude.toString(), location.coords.longitude.toString());
        console.log('codes', codes);
        await this.setPostalLocation(location.coords.latitude,
          location.coords.longitude,
          codes.postalCodes[0].adminName1,
          codes.postalCodes[0].postalCode
        )
      }
    } catch (error) {
      console.error(error);
      return error;
    }
  }

  async setNewLocation() {
    let options: GeolocationOptions = {
      enableHighAccuracy: true,
      timeout: 10000,
      maximumAge: 100000
    }
    let location = await this.geolocation.getCurrentPosition(options);
    console.log('location', location);
    this.latitude = location.coords.latitude;
    this.longitude = location.coords.longitude;
    let codes = await this.generatePostalCodes(location.coords.latitude.toString(), location.coords.longitude.toString());
    console.log('codes', codes);
    await this.setPostalLocation(location.coords.latitude,
      location.coords.longitude,
      codes.postalCodes[0].postalCode,
      codes.postalCodes[0].adminName1
    )
    // this.address = codes.postalCodes[0].adminName1;
    // this.postalCode = codes.postalCodes[0].postalCode;
    // let userLocation = {
    //   lat: location.coords.latitude,
    //   lng: location.coords.longitude,
    //   address: codes.postalCodes[0].adminName1,
    //   postalCode: codes.postalCodes[0].postalCode
    // }
    // await this.storage.set('userLocation', userLocation);
  }

  geocode(postalCode?: string) {
    postalCode = this.postalCode;
    let body = new URLSearchParams;
    // let latlng = lat + ',' + lng

    // body.set('latlng', latlng)
    body.set('address', postalCode);
    body.set('result_type', 'administrative_area_level_1')
    body.set('key', 'AIzaSyByvGBV3BQRMhOKAhUs3lc4IOPpewS2zDE')

    let options = new RequestOptions();
    options.search = body;
    return this.http.get(`https://maps.googleapis.com/maps/api/geocode/json`, options)
      .map(this.api.returnResponse)
      .catch(this.api.catchError)

  }

  async setPostalLocation(lat, lng, postalCode, address) {
    this.latitude = lat;
    this.longitude = lng;
    this.address = address;
    this.postalCode = postalCode;
    let userLocation = {
      lat: lat,
      lng: lng,
      address: address,
      postalCode: postalCode
    }
    await this.storage.set('userLocation', userLocation);
  }

  generatePostalCodes(lat: string, lng: string) {
    let body = new URLSearchParams;
    console.log(lat)
    body.set('lat', lat);
    body.set('lng', lng);
    body.set('username', 'anasmunir');

    let options = new RequestOptions();
    options.search = body;

    return this.http.get('http://api.geonames.org/findNearbyPostalCodesJSON', options)
      .toPromise()
      .then(this.api.returnResponse)
      .catch(error => { return error })
    // .map(this.api.returnResponse)
    // .catch(this.api.catchError)
    // .subscribe(resp => {
    //   console.log(resp)
    //   this.address = resp[0].adminName1;
    //   this.postalCode = resp[0].postalCode;
    // })
  }
}
