import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { FileTransfer, FileTransferObject, FileUploadOptions } from "@ionic-native/file-transfer";
// import { File } from "@ionic-native/file";
import { ApiProvider } from "../api/api";
import { AuthServiceProvider } from "../auth-service/auth-service";
// declare var cordova: any;


@Injectable()
export class PostingServiceProvider {

  public title: string = '';
  public images = [];
  public path: string = '';
  public name: string = '';
  public selectedCategory = { categoryId: 0, category: '' };
  public condition: number = 0;
  public conditionDescription = {
    0: 'Other (see description)',
    1: 'For Parts',
    2: 'User (normal wear)',
    3: 'Open Box (never used)',
    4: 'Reconditioned/Certified',
    5: 'New (never used)'
  };
  public description: string = '';
  public price: number = 5;
  public priceFixed: boolean = false;
  public latitude: number = 53.1232;
  public longitude: number = 53.1232;
  public zip = "5400"
  public offerId;
  private promiseArray = [];
  public offersList = [];
  public filters = {
    filter: '3',
    radius: 2,
    firmPrice: '',
    featured: '',
    priceFrom: '',
    priceTo: '',
    sortBy: 'newest'
  }
  public newPostAdded: boolean = false;

  constructor(
    public http: Http,
    private transfer: FileTransfer,
    private as: AuthServiceProvider,
    private api: ApiProvider,
    // private file: File
  ) {

    console.log('Hello PostingServiceProvider Provider');
  }

  picupload() {
    console.log("offerId", this.offerId)
    const postImageUrl: string = this.api.url + "/post/new_post_image_app"
    this.images.map(image => {
      let options: FileUploadOptions = {
        fileKey: "post_images",
        fileName: image.name,
        chunkedMode: false,
        mimeType: "image/jpg",
        httpMethod: "POST",
        params: {
          "userId": this.as.isLoginSubject.value.userId,
          "authToken": this.as.authToken,
          "offerId": this.offerId,
        }
      };
      const fileTransfer: FileTransferObject = this.transfer.create();
      let myPromise = fileTransfer.upload(image.url, postImageUrl, options, true)
      this.promiseArray.push(myPromise)
    })

    return Promise.all(this.promiseArray).then(values => {
      console.log(values)
    }).catch(err => {
      console.error(err)
    })
  }

  newPost(latitude?, longitude?) {
    let firmPrice = this.priceFixed ? "yes" : "no";
    let body = {
      userId: this.as.isLoginSubject.value.userId,
      title: this.title,
      description: this.description,
      condition: this.condition,
      price: this.price,
      firmPrice: firmPrice,
      zip: this.zip,
      latitude: latitude,
      longitude: longitude,
      authToken: this.as.authToken,
      category: this.selectedCategory.categoryId
    }

    let response = this.api.post('post/new_post_app', body).share()
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
    // response
    //   .map(this.api.returnResponse)
    //   .catch(this.api.catchError)
    //   .subscribe(async resp => {
    //     console.log(resp);
    //     this.offerId = resp.data.offerId
    //     await this.picupload()
    //   })
  }

  clearData() {
    this.title = '';
    this.images = [];
    this.path = '';
    this.name = '';
    this.selectedCategory = { categoryId: 0, category: '' };
    // this.condition = 0;
    this.description = '';
    this.price = 5;
    this.priceFixed = false;
    this.latitude = 53.1232;
    this.longitude = 53.1232;
    this.zip = "5400"
    this.offerId;
    this.promiseArray = [];
  }
}
