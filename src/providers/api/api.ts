import { Injectable } from '@angular/core';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from "rxjs";


@Injectable()
export class ApiProvider {

  // public url: string = "https://proday24.com/proday_dev/proday_api";
  public url: string = "https://proday24.com/proday_dev/proday_api/index.php";

  constructor(private http: Http) {
    // console.log('Hello ApiProvider Provider');
  }

  get(endpoint: string, params?: any, options?: RequestOptions) {
    if (!options) {
      options = new RequestOptions();
    }

    // Support easy query params for GET requests
    if (params) {
      let p = new URLSearchParams();
      for (let k in params) {
        p.set(k, params[k]);
      }

      options.search = !options.search && p || options.search;
    }

    return this.http.get(this.url + '/' + endpoint, options);
  }

  post(endpoint: string, body?: any, options?: RequestOptions) {
    console.log(this.url + '/' + endpoint);
    if (body) {
      let urlBody = new URLSearchParams();
      for (let k in body) {
        urlBody.set(k, body[k]);
      }
      console.log("urlBody", urlBody);
      return this.http.post(this.url + '/' + endpoint, urlBody);
    } else {
      return this.http.post(this.url + '/' + endpoint, body);
    }

  }

  put(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.put(this.url + '/' + endpoint, body, options);
  }

  delete(endpoint: string, options?: RequestOptions) {
    return this.http.delete(this.url + '/' + endpoint, options);
  }

  patch(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.put(this.url + '/' + endpoint, body, options);
  }

  public returnResponse(res) {
    return res.json()
  }

  public catchError(error) {
    // console.log(error);
    return Observable.throw(error || error.json() || "some error in http get");
    // return Promise.reject(error.json().error || error || "some error in http post");
  }
}
