import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { ApiProvider } from "../api/api";
import { AuthServiceProvider } from "../auth-service/auth-service";
import { FileTransfer, FileTransferObject, FileUploadOptions } from "@ionic-native/file-transfer";


@Injectable()
export class UserServiceProvider {

  constructor(
    private api: ApiProvider,
    private as: AuthServiceProvider,
    private transfer: FileTransfer) {
    // console.log('Hello UserServiceProvider Provider');
  }

  getUserInfo(userId?) {
    let body;
    if (userId) {
      body = {
        authToken: this.as.authToken,
        userId: userId
      }
    } else {
      body = {
        authToken: this.as.authToken,
        userId: this.as.isLoginSubject.value.userId
      }
    }

    let response = this.api.post('user/get_info', body)
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

  async updatePhoto(imageObject) {
    const postImageUrl: string = this.api.url + "/user/update_photo";
    let options: FileUploadOptions = {
      fileKey: "profile_image",
      fileName: imageObject.name,
      chunkedMode: false,
      mimeType: "image/jpg",
      httpMethod: "POST",
      params: {
        "userId": this.as.isLoginSubject.value.userId,
        "authToken": this.as.authToken
      }
    };
    try {
      const fileTransfer: FileTransferObject = this.transfer.create();
      const photoUpload = await fileTransfer.upload(imageObject.url, postImageUrl, options, true)
      return photoUpload
    } catch (error) {
      throw error;
    }
  }

  updateInfo(info) {
    let body = info;
    body['authToken'] = this.as.authToken;
    body['userId'] = this.as.isLoginSubject.value.userId;
    let response = this.api.post('user/update_info', body)
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

}
