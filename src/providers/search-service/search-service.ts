import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ApiProvider } from "../api/api";
import { AuthServiceProvider } from "../auth-service/auth-service";
import { LocationServiceProvider } from "../location-service/location-service";
import { Observable } from "rxjs";
import 'rxjs/add/operator/map';


@Injectable()
export class SearchServiceProvider {

  constructor(
    public http: Http,
    private api: ApiProvider,
    private as: AuthServiceProvider,
    private locationService: LocationServiceProvider) {

    console.log('Hello SearchServiceProvider Provider');
  }

  search(terms: Observable<string>, debounceDuration = 400) {
    return terms.debounceTime(debounceDuration)
      .distinctUntilChanged()
      .switchMap(term => this.rawSearch(term));
  }

  rawSearch(term: string, category?) {
    let coordinates = this.locationService.latitude + ',' + this.locationService.longitude
    let body = {
      authToken: this.as.authToken,
      coordinates: coordinates,
      category: category,
      keyword: term,
      limit: '10000',
      offset: '0',
      radius: '10000',
    }
    return this.api.post('listing/search_offers', body)
      .map(this.api.returnResponse);
  }

}
