import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from "@ionic-native/camera";
import { FilePath } from "@ionic-native/file-path";
import { File } from "@ionic-native/file";
import 'rxjs/add/operator/map';
import { ImageResizer, ImageResizerOptions } from "@ionic-native/image-resizer";


@Injectable()
export class CameraServiceProvider {

  constructor(
    private camera: Camera,
    private filePath: FilePath,
    private imageResizer: ImageResizer,
    private file: File) {
    console.log('Hello CameraServiceProvider Provider');
  }

  async takeAPicture() {
    let options: CameraOptions = {
      quality: 50,
      // saveToPhotoAlbum: true,
      // allowEdit: true,
      // targetHeight: 250,
      // targetWidth: 250,
      correctOrientation: true,
      saveToPhotoAlbum: true,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA
    }
    try {
      const imageData = await this.camera.getPicture(options).catch(error => {throw error})
      console.log("imageData camera-service", imageData)
      const imageObject = await this.resolveImageUrl(imageData, 'CAMERA').catch(error => {throw error})
      const resizedImage = await this.resizeImage(imageObject.url);
      imageObject.base64Image = resizedImage;
      return imageObject
    } catch (error) {
      console.error(`error in camera service ${error}`)
      throw `error in camera service ${error}`
    }
  }

  async chooseFromGallery() {
    let options: CameraOptions = {
      quality: 50,
      // targetHeight: 250,
      // targetWidth: 250,
      // allowEdit: true,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }
    try {
      const imageData = await this.camera.getPicture(options).catch(error => {throw error})
      console.log("imageData camera-service", imageData)
      const imageObject = await this.resolveImageUrl(imageData, 'PHOTOLIBRARY').catch(error => {throw error})
      const resizedImage = await this.resizeImage(imageObject.url);
      imageObject.base64Image = resizedImage;
      return imageObject
    } catch (error) {
      console.error(`error in photo service ${error}`)
      throw `error in photo service ${error}`
    }
  }

  async resolveImageUrl(imageData, sourceType) {
    if (sourceType === 'PHOTOLIBRARY') {
      const filePath = await this.filePath.resolveNativePath(imageData)
      let pathFile: string = filePath.substring(0, filePath.lastIndexOf('/') + 1);
      let name = filePath.substring(filePath.lastIndexOf('/') + 1);
      console.log("filepath from plugin:", filePath)
      let nativeURL = await this.file.copyFile(pathFile, name, this.file.externalApplicationStorageDirectory, name)
      let base64Image = nativeURL.nativeURL;
      let imageObject = { base64Image: base64Image, path: pathFile, name: name, url: nativeURL.nativeURL }
      return imageObject
    } else {
      let path = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      let name = imageData.substring(imageData.lastIndexOf('/') + 1);
      let nativeURL = await this.file.copyFile(path, name, this.file.externalApplicationStorageDirectory, name)
      let base64Image = nativeURL.nativeURL;
      let imageObject = { base64Image: base64Image, path: path, name: name, url: nativeURL.nativeURL }
      return imageObject
    }
  }

  async resizeImage(uri) {
    let options: ImageResizerOptions = {
      uri: uri,
      folderName: 'Protonet',
      quality: 50,
      width: 1280,
      height: 720
    };
    try {
      let filePath: string = await this.imageResizer.resize(options);
      console.log('resized image filepath', filePath);
      return filePath;
    } catch (error) {
      console.error(error);
    }
  }

}
