import { Injectable } from '@angular/core';
import { ApiProvider } from "../api/api";
// import { UserServiceProvider } from "../user-service/user-service";
import 'rxjs/add/operator/share';
import { Observable, BehaviorSubject } from "rxjs";
import { Facebook } from "@ionic-native/facebook";
import { Storage } from "@ionic/storage";

export class UserInfo {
  userId: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  dateCreated: string;
  image: string;
  socialLogin: boolean;
}

@Injectable()
export class AuthServiceProvider {
  _user: UserInfo;
  userId: any;
  public authToken = '';
  isLoginSubject = new BehaviorSubject(null);

  constructor(
    private api: ApiProvider,
    private fb: Facebook,
    // public userService: UserServiceProvider,
    private storage: Storage) {
    // console.log('Hello AuthServiceProvider Provider');
  }

  login(email: string, password?: string) {
    let body = { email: email, password: password || null, authToken: this.authToken }
    let seq = this.api.post('user/login', body).share();
    console.log("email", body)
    return seq
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

  socialLogin(socialData) {
    let body = {
      authToken: this.authToken,
      email: socialData.email,
      firstName: socialData.first_name,
      lastName: socialData.last_name,
      phone: ''
    }
    let resp = this.api.post('user/login_social', body);
    return resp
      .map(this.api.returnResponse)
      .catch(this.api.catchError);
  }

  signup(formData) {
    let body = {
      email: formData.email,
      password: formData.password || '',
      firstName: formData.firstName || formData.first_name,
      lastName: formData.lastName || formData.last_name,
      phone: formData.phone || '',
      authToken: this.authToken
    };
    console.log("signup", body);
    let seq = this.api.post('user/register', body)

    return seq
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

  async logout(userId) {
    let body = {
      userId: userId,
      authToken: this.authToken
    }
    console.log("logout", body)
    await this.storage.clear()
    if (this.isLoginSubject.getValue().socialLogin) {
      await this.fb.logout();
    }
    this.isLoginSubject.next(null)
    return this.api.post('user/logout', body).toPromise()
  }
  getAuthToken() {
    let seq = this.api.post('startup/get_app_startup').share();

    return seq
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

  /*  private logResponse(res) {
     console.log(res.json());
   } */

  isLoggedIn(): Observable<any> {
    return this.isLoginSubject.asObservable().share()
  }

  async updateLoginSubject(userInfo) {
    for (var userProp in userInfo) {
      this.isLoginSubject.value[userProp] = userInfo[userProp];
    }
    console.log('this.isLoginSubject.value', this.isLoginSubject.value);
    return await this.storage.set('user', this.isLoginSubject.value)
    // console.log("user from updateLoginSubject", user)
    // this.isLoginSubject.next(Object.assign({}, user, { isLoggedIn: true }))
  }

  async _initialiseUser(userData, image?, socialLogin?: boolean) {
    try {
      console.log('userData', userData);
      const resp = await this.getUserInfo(userData.userId).toPromise()
      if (resp.data.profile) {
        this._user = {
          userId: userData.userId,
          firstName: resp.data.profile.first_name,
          phone: userData.phone,
          lastName: resp.data.profile.last_name,
          email: resp.data.profile.email,
          image: image ? image : resp.data.profile.image,
          dateCreated: resp.data.profile.date_created,
          socialLogin: socialLogin
        }
      } else {
        this._user = {
          userId: userData.userId,
          firstName: userData.name,
          phone: userData.phone,
          lastName: null,
          email: null,
          image: null,
          dateCreated: null,
          socialLogin: socialLogin
        }
      }

      await this.storage.set('user', this._user)
      const user: UserInfo = await this.storage.get('user')
      console.log("user", user)
      this.isLoginSubject.next(Object.assign({}, this._user))
    } catch (error) {
      console.error(error)
    }
  }

  _setAuthToken(token) {
    return this.authToken = token;
  }


  async hasToken() {
    const user: UserInfo = await this.storage.get('user')
    if (user) {
      console.log(user)
      this._user = user
      this.isLoginSubject.next(Object.assign({}, this._user, { isLoggedIn: true }))
      return true
    } else {
      return false
    }
  }

  getUserInfo(userId?) {
    let body;
    if (userId) {
      body = {
        authToken: this.authToken,
        userId: userId
      }
    } else {
      body = {
        authToken: this.authToken,
        userId: this.isLoginSubject.value.userId
      }
    }

    let response = this.api.post('user/get_info', body)
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

}
