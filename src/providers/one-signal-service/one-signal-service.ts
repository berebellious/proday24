import { Injectable } from '@angular/core';
import { OneSignal } from "@ionic-native/onesignal";
import { Platform } from 'ionic-angular';
import 'rxjs/add/operator/map';


@Injectable()
export class OneSignalServiceProvider {

  constructor(private _SIGNAL: OneSignal, public platform: Platform) {
  }
  initialiseOneSignal(): void {
    if (this.platform.is('ios')) {
      console.log("platform ios");
      var iosSettings = {};
      iosSettings["kOSSettingsKeyAutoPrompt"] = true;
      iosSettings["kOSSettingsKeyInAppLaunchURL"] = false;

      this._SIGNAL.startInit('a2a99a97-565e-4b06-9cdf-decf3ddabeeb').iOSSettings(iosSettings);
    }
    else if (this.platform.is('android')) {
      console.log("platform android");
      this._SIGNAL.startInit('a2a99a97-565e-4b06-9cdf-decf3ddabeeb', '787787016227');
    }
    this._SIGNAL.inFocusDisplaying(this._SIGNAL.OSInFocusDisplayOption.Notification);
    this._SIGNAL.setSubscription(true);
    this._SIGNAL.handleNotificationReceived().subscribe(resp => {
      // your code after Notification received.
      console.log('notification recieved', resp);
    });
    this._SIGNAL.handleNotificationOpened().subscribe(resp => {
      // your code to handle after Notification opened
      console.log('handleNotificationOpened', resp);
    });
    this._SIGNAL.endInit();
  }

}
