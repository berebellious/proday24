import { Injectable } from '@angular/core';
// import { Http } from '@angular/http';
import { AuthServiceProvider } from "../auth-service/auth-service";
import { LocationServiceProvider } from "../location-service/location-service";
import { ApiProvider } from "../api/api";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/publishReplay';
// import 'rxjs/add/operator/';
import 'rxjs/add/operator/catch';
import "rxjs";
// import { Observable } from "rxjs";


@Injectable()
export class DataServiceProvider {

  public sellingDataLoaded: boolean = false;
  public buyingData: any;
  constructor(
    // private http: Http,
    private api: ApiProvider,
    private as: AuthServiceProvider,
    private locationService: LocationServiceProvider) {

    // console.log('Hello DataServiceProvider Provider');
  }

  getCategories() {
    let authToken = this.as.authToken;
    console.log('authToken', authToken);
    let body = {
      authToken: authToken,
      offset: 0,
      limit: 20
    }

    // return this.postRequest('post/post_bid', body)
    let response = this.api.post('listing/get_categories', body).share();

    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

  getOffers(category, filter, radius?, firmPrice?, featured?, priceFrom?, priceTo?) {
    let coordinates = this.locationService.latitude + ',' + this.locationService.longitude;
    const userId = this.as.isLoginSubject.value ? this.as.isLoginSubject.value.userId : null
    let body = {
      authToken: this.as.authToken,
      userId: userId,
      coordinates: coordinates,
      category: category,
      offset: '0',
      limit: '10000',
      filter: filter,
      radius: radius + '000' || '2000',
      firmPrice: firmPrice || '',
      featured: featured || '',
      priceRangeFrom: priceFrom || '',
      priceRangeTo: priceTo || '',
    }

    // return this.postRequest('listing/get_offers', body)
    console.log("getOffers body",body);
    let response = this.api.post('listing/get_offers', body)

    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

  getOfferDetail(offerId) {
    let coordinates = this.locationService.latitude + ',' + this.locationService.longitude
    let body = {
      authToken: this.as.authToken,
      coordinates: coordinates,
      offerId: offerId,
      userId: this.as.isLoginSubject.value.userId
    }

    // return this.postRequest('listing/get_single_offer', body)
    let response = this.api.post('listing/get_single_offer', body)

    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

  postAd(userId, data) {
    let body = {
      userId: this.as.isLoginSubject.value.userId,
      title: data.title,
      description: data.description,
      condition: data.condition,
      price: data.price,
      firmPrice: data.firmPrice,
      zip: data.zip,
      latitude: data.latitude,
      longitude: data.longitude,
      post_images: data.images,
      authToken: this.as.authToken,
      featured: "no"
    }
    return this.postRequest('post/new_post', body)
    // let response = this.api.post('post/new_post', body).share()

    // return response
    //   .map(this.api.returnResponse)
    //   .catch(this.api.catchError)
  }

  makeOffer(offerId, receiver, amount) {
    let body = {
      authToken: this.as.authToken,
      offerId: offerId,
      sender: this.as.isLoginSubject.value.userId,
      receiver: receiver,
      amount: amount
    }
    return this.postRequest('post/post_bid', body)
    // let response = this.api.post('post/post_bid', body).share()

    // return response
    //   .map(this.api.returnResponse)
    //   .catch(this.api.catchError)
  }

  myOffersSelling() {
    let body = {
      authToken: this.as.authToken,
      userId: this.as.isLoginSubject.value.userId
    }
    // return this.postRequest('listing/get_selling', body);
    let response = this.api.post('listing/get_selling', body);
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
    // if (!this.sellingData) {

    //   this.sellingData = this.api.post('listing/get_selling', body)
    //     .map(this.api.returnResponse)
    //     .publishReplay(1)
    //     .refCount().take(1)
    //     .catch(this.api.catchError)
    // }
    // return this.sellingData
  }

  myOffersBuying() {
    let body = {
      authToken: this.as.authToken,
      userId: this.as.isLoginSubject.value.userId
    }

    if (!this.buyingData) {
      this.buyingData = this.api.post('listing/get_buying', body)
        .map(this.api.returnResponse)
        .publishReplay(1)
        .refCount().take(1)
        .catch(this.api.catchError)
    }
    return this.buyingData
  }

  getChatList() {
    let body = {
      authToken: this.as.authToken,
      userId: this.as.isLoginSubject.value.userId
    }
    let response = this.api.post('chat/list', body)
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

  messageSeller(otherId, offerId, message: string) {
    let body = {
      authToken: this.as.authToken,
      userId: this.as.isLoginSubject.value.userId,
      otherId: otherId,
      offerId: offerId,
      message: message
    }
    let response = this.api.post('chat/send_on_offer', body);
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

  getOfferMsgList() {
    let body = {
      authToken: this.as.authToken,
      userId: this.as.isLoginSubject.value.userId
    }
    let response = this.api.post('chat/list_on_offer', body)
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

  getSellerProfile(profileId) {
    let body = {
      authToken: this.as.authToken,
      userId: this.as.isLoginSubject.value.userId || '',
      profileId: profileId
    }
    let response = this.api.post('listing/seller_profile', body);
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

  follow(otherId) {
    let body = {
      authToken: this.as.authToken,
      userId: this.as.isLoginSubject.value.userId || '',
      otherId: otherId
    }
    let response = this.api.post('post/toggle_following', body);
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

  makePayment(amount, token, offerId) {
    let body = {
      authToken: this.as.authToken,
      userId: this.as.isLoginSubject.value.userId || '',
      offerId: offerId,
      amount: amount,
      stripeToken: token
    }
    let response = this.api.post('payment/make_payment', body);
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError)
  }

  getMarkAsSoldList(offerId) {
    let body = {
      authToken: this.as.authToken,
      userId: this.as.isLoginSubject.value.userId,
      offerId: offerId
    }
    let response = this.api.post('listing/get_mark_as_sold', body);
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError);
  }

  toggleWatchList(offerId) {
    let body = {
      authToken: this.as.authToken,
      userId: this.as.isLoginSubject.value.userId,
      offerId: offerId
    }
    let response = this.api.post('post/toggle_watchlist', body);
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError);
  }

  getWatchList() {
    let body = {
      authToken: this.as.authToken,
      userId: this.as.isLoginSubject.value.userId
    }
    let response = this.api.post('listing/get_watchlist', body);
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError);
  }

  report(offerId, reason: string) {
    let body = {
      authToken: this.as.authToken,
      userId: this.as.isLoginSubject.value.userId,
      offerId: offerId,
      reason: reason
    };
    let response = this.api.post('post/post_report', body);
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError);
  }

  sell(offerId, otherId) {
    let body = {
      authToken: this.as.authToken,
      userId: this.as.isLoginSubject.value.userId,
      offerId: offerId,
      otherId: otherId
    };
    let response = this.api.post(`post/mark_status_sold`, body);
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError);
  }

  review(receiver, rating) {
    let body = {
      authToken: this.as.authToken,
      sender: this.as.isLoginSubject.value.userId,
      receiver: receiver,
      review: '',
      rating: rating
    };
    let response = this.api.post('post/post_review', body);
    return response
      .map(this.api.returnResponse)
      .catch(this.api.catchError);
  }

  private postRequest(url: string, body?) {

    let response = this.api.post(url, body)
      .map(this.api.returnResponse)
      .publishReplay(1)
      .refCount()
      .catch(this.api.catchError)
    return response

  }



}
