import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Events } from 'ionic-angular';
import { ApiProvider } from "../api/api";
import { AuthServiceProvider } from "../auth-service/auth-service";
// import { Observable } from "rxjs";

import 'rxjs/add/operator/toPromise';

export class ChatMessage {
    // messageId: string;
    // userId: string;
    mtime: number | string;
    message: string;
    side: string;
    // status: string;
}

@Injectable()
export class ToUserInfo {
    userId: string;
    userName: string;
    userImgUrl: string;
}

@Injectable()
export class ChatServiceProvider {

    public itemDetails: any;
    public image: any;
    public name: any;

    constructor(public http: Http,
        public events: Events,
        private api: ApiProvider,
        private toUserInfo: ToUserInfo,
        private as: AuthServiceProvider) {
    }

    mockNewMsg(offerId) {

        let body = {
            authToken: this.as.authToken,
            userId: this.as.isLoginSubject.value.userId,
            otherId: this.toUserInfo.userId,
            offerId: offerId
        }
        console.log(body);

        return this.api.post('chat/new_messages_on_offer', body)
            .map(this.api.returnResponse)
            .catch(this.api.catchError)
    }

    mockNewAlertMsg() {

        let body = {
            authToken: this.as.authToken,
            userId: this.as.isLoginSubject.value.userId,
            otherId: this.toUserInfo.userId,
            // offerId: this.chatMessage.offerId
        }
        console.log(body);

        return this.api.post('chat/new_messages', body)
            .map(this.api.returnResponse)
            .catch(this.api.catchError)
    }

    getMsgList(offerId): Promise</* ChatMessage[] */any> {
        let body = {
            authToken: this.as.authToken,
            userId: this.as.isLoginSubject.value.userId,
            otherId: this.toUserInfo.userId,
            offerId: offerId
        }
        const msgListUrl = 'chat/messages_on_offer';
        return this.api.post(msgListUrl, body)
            .toPromise()
            .then(response => response.json() /* as ChatMessage[] */)
            .catch(err => Promise.reject(err || 'err'));
    }

    getMsgAlertList() {
        let body = {
            authToken: this.as.authToken,
            userId: this.as.isLoginSubject.value.userId,
            otherId: this.toUserInfo.userId,
            // offerId: this.chatMessage.offerId
        }
        const msgListUrl = 'chat/messages';
        return this.api.post(msgListUrl, body)
            .toPromise()
            .then(response => response.json() /* as ChatMessage[] */)
            .catch(err => Promise.reject(err || 'err'));
    }

    sendMsg(msg, offerId) {
        // return new Promise(resolve => setTimeout(() => resolve(msg), Math.random() * 1000))
        //     .then(() => this.mockNewMsg());
        let body = {
            authToken: this.as.authToken,
            userId: this.as.isLoginSubject.value.userId,
            otherId: this.toUserInfo.userId,
            offerId: offerId,
            message: msg.message
        }
        const msgListUrl = 'chat/send_on_offer';
        return this.api.post(msgListUrl, body)
            .toPromise()
            .then(response => response.json() /* as ChatMessage[] */)
            .catch(err => Promise.reject(err || 'err'));
    }

    sendAlertMsg(msg) {
        // return new Promise(resolve => setTimeout(() => resolve(msg), Math.random() * 1000))
        //     .then(() => this.mockNewMsg());
        let body = {
            authToken: this.as.authToken,
            userId: this.as.isLoginSubject.value.userId,
            otherId: this.toUserInfo.userId,
            // offerId: this.chatMessage.offerId,
            message: msg.message
        }
        const msgListUrl = 'chat/send';
        return this.api.post(msgListUrl, body)
            .toPromise()
            .then(response => response.json() /* as ChatMessage[] */)
            .catch(err => Promise.reject(err || 'err'));
    }

    getChatList() {
        let body = {
            authToken: this.as.authToken,
            userId: this.as.isLoginSubject.value.userId
        }
        let response = this.api.post('chat/list', body)
        return response
            .map(this.api.returnResponse)
            .catch(this.api.catchError)
        // .toPromise()
        // .then(response => response.json() /* as ChatMessage[] */)
        // .catch(err => Promise.reject(err || 'err'));
    }

    getOfferChatList(offerId) {
        let body = {
            authToken: this.as.authToken,
            userId: this.as.isLoginSubject.value.userId,
            offerId: offerId,
        }
        let response = this.api.post('chat/list_on_offer', body)
        return response
            .map(this.api.returnResponse)
            .catch(this.api.catchError)
        // .toPromise()
        // .then(response => response.json() /* as ChatMessage[] */)
        // .catch(err => Promise.reject(err || 'err'));
    }

    messageSeller(otherId, offerId, message: string) {
        let body = {
            authToken: this.as.authToken,
            userId: this.as.isLoginSubject.value.userId,
            otherId: otherId,
            offerId: offerId,
            message: message
        }
        let response = this.api.post('chat/send_on_offer', body);
        return response
            .map(this.api.returnResponse)
            .catch(this.api.catchError)
    }
}
