import { NgModule } from '@angular/core';
import { EmojiPickerComponent } from './emoji-picker/emoji-picker';
import { GridLayoutComponent } from './grid-layout/grid-layout';
import { GoogleMapComponent } from './google-map/google-map';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
    declarations: [
        EmojiPickerComponent,
        GridLayoutComponent,
        GoogleMapComponent
    ],
    imports: [CommonModule],
    exports: [
        EmojiPickerComponent,
        GridLayoutComponent,
        GoogleMapComponent
    ]
})
export class ComponentsModule { }
