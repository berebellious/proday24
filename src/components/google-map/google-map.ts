import { Component, Input, ViewChild, ElementRef, OnInit } from '@angular/core';

declare var google;

@Component({
  selector: 'google-map',
  templateUrl: 'google-map.html'
})
export class GoogleMapComponent implements OnInit {

  @Input('lat') lat: any;
  @Input('lng') lng: any;
  @ViewChild('mapContainer') mapContainer: ElementRef;
  map: any;

  ngOnInit() {
    let latLng = new google.maps.LatLng(this.lat, this.lng);

    let mapOptions = {
      center: latLng,
      disableDefaultUI: true,
      zoom: 15,
      gestureHandling: 'cooperative',
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: [
        {
          "featureType": "landscape",
          "stylers": [
            {
              "hue": "#FFA800"
            },
            {
              "saturation": 0
            },
            {
              "lightness": 0
            },
            {
              "gamma": 1
            }
          ]
        },
        {
          "featureType": "road.highway",
          "stylers": [
            {
              "hue": "#53FF00"
            },
            {
              "saturation": -73
            },
            {
              "lightness": 40
            },
            {
              "gamma": 1
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "stylers": [
            {
              "hue": "#FBFF00"
            },
            {
              "saturation": 0
            },
            {
              "lightness": 0
            },
            {
              "gamma": 1
            }
          ]
        },
        {
          "featureType": "road.local",
          "stylers": [
            {
              "hue": "#00FFFD"
            },
            {
              "saturation": 0
            },
            {
              "lightness": 30
            },
            {
              "gamma": 1
            }
          ]
        },
        {
          "featureType": "water",
          "stylers": [
            {
              "hue": "#00BFFF"
            },
            {
              "saturation": 6
            },
            {
              "lightness": 8
            },
            {
              "gamma": 1
            }
          ]
        },
        {
          "featureType": "poi",
          "stylers": [
            {
              "hue": "#679714"
            },
            {
              "saturation": 33.4
            },
            {
              "lightness": -25.4
            },
            {
              "gamma": 1
            }
          ]
        }
      ]
    }
    this.map = new google.maps.Map(this.mapContainer.nativeElement, mapOptions);
    // var marker = new google.maps.Marker({
    //   position: latLng,
    //   map: this.map
    // });
    var cityCircle = new google.maps.Circle({
      strokeColor: '#00a651',
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: '#00a651',
      fillOpacity: 0.35,
      map: this.map,
      center: latLng,
      radius: 150
    });
  }
  constructor() {

  }
}